package ch.dxc.hackerdy.forgotpassword;

public interface PasswordResetCallback
{
    void onStart();

    void onSuccess();

    void onFailure(String msg);
}