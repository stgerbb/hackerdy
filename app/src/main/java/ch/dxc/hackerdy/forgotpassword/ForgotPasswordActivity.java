package ch.dxc.hackerdy.forgotpassword;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.snackbar.Snackbar;

import ch.dxc.hackerdy.R;
import ch.dxc.hackerdy.databinding.ActivityForgotPasswordBinding;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener
{
    private ActivityForgotPasswordBinding mBinding;
    private ForgotPasswordViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password);
        mViewModel = ViewModelProviders.of(this).get(ForgotPasswordViewModel.class);
        mBinding.setViewModel(mViewModel);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mBinding.buttonSubmit.setOnClickListener(this);

        mBinding.inputEmail.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void afterTextChanged(Editable editable)
            {
                if (mBinding.inputLayoutEmail.isErrorEnabled())
                {
                    mBinding.inputLayoutEmail.setErrorEnabled(false);
                }
            }
        });

        mViewModel.getEmailError().observe(this, new Observer<Integer>()
        {
            @Override
            public void onChanged(Integer resId)
            {
                mBinding.inputLayoutEmail.setError(getString(resId));
            }
        });

        mViewModel.getPasswordResetSent().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean sent)
            {
                Intent data = new Intent();
                data.putExtra("email", mBinding.inputEmail.getText().toString());
                setResult(Activity.RESULT_OK, data);
                finish();
            }
        });

        mViewModel.getRepositoryError().observe(this, new Observer<String>()
        {
            @Override
            public void onChanged(String msg)
            {
                Snackbar.make(mBinding.activityForgotPassword, msg, Snackbar.LENGTH_LONG).show();
            }
        });

        mViewModel.isLoading().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean loading)
            {
                if (loading)
                {
                    mBinding.progressbar.bringToFront();
                    mBinding.progressbar.setVisibility(View.VISIBLE);
                    mBinding.inputEmail.setEnabled(false);
                    mBinding.buttonSubmit.setEnabled(false);
                }
                else
                {
                    mBinding.progressbar.setVisibility(View.GONE);
                    mBinding.inputEmail.setEnabled(true);
                    mBinding.buttonSubmit.setEnabled(true);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.button_submit:
                mViewModel.sendPasswordReset();
                break;
        }
    }
}