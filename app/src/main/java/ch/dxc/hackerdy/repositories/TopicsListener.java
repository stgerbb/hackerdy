package ch.dxc.hackerdy.repositories;

import java.util.List;

public interface TopicsListener
{
    void get(List<String >topics);
}