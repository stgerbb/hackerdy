package ch.dxc.hackerdy.gamemode;

import android.net.Uri;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.auth.FirebaseUser;

import ch.dxc.hackerdy.repositories.FirebaseRepository;

public class GameModeViewModel extends ViewModel
{
    private MutableLiveData<Uri> mPhoto = new MutableLiveData<>();
    private MutableLiveData<Boolean> mLoadingAvatarSinglePlayer = new MutableLiveData<>();
    private MutableLiveData<Boolean> mLoadingAvatarBattleMode = new MutableLiveData<>();

    private FirebaseRepository mRepository = new FirebaseRepository();

    public GameModeViewModel()
    {
        mLoadingAvatarSinglePlayer.setValue(true);
        mLoadingAvatarBattleMode.setValue(true);

        FirebaseUser user = mRepository.getUser();
        mPhoto.setValue(user.getPhotoUrl());
    }

    LiveData<Uri> getPhoto()
    {
        return mPhoto;
    }

    LiveData<Boolean> isLoadingAvatarSinglePlayer ()
    {
        return mLoadingAvatarSinglePlayer;
    }

    LiveData<Boolean> isLoadingAvatarBattleMode()
    {
        return mLoadingAvatarBattleMode;
    }

    void setLoadingAvatarSinglePlayer(Boolean bool)
    {
        mLoadingAvatarSinglePlayer.setValue(bool);
    }
    void setLoadingAvatarBattleMode(Boolean bool)
    {
        mLoadingAvatarBattleMode.setValue(bool);
    }
}