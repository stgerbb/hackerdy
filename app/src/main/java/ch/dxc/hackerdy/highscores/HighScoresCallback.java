package ch.dxc.hackerdy.highscores;

public interface HighScoresCallback
{
    void addHighScoreListItemToList(HighScoresListItem highScoresListItem);
}
