package ch.dxc.hackerdy.highscores;

import android.util.Log;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import ch.dxc.hackerdy.repositories.FirebaseRepository;

public class HighScoresLiveData extends LiveData<List<HighScoresListItem>> implements EventListener<QuerySnapshot>, HighScoresCallback
{
    private Query mQuery;
    private ListenerRegistration mListener;
    private FirebaseRepository mRepository = new FirebaseRepository();
    private List<HighScoresListItem> mHighScoresListItemList = new ArrayList<>();
    private long mUserCount = 0;

    public HighScoresLiveData(Query query)
    {
        mQuery = query;
    }

    @Override
    protected void onActive()
    {
        mListener = mQuery.addSnapshotListener(this);
    }

    @Override
    protected void onInactive()
    {
        mListener.remove();
    }

    @Override
    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e)
    {
        if (!queryDocumentSnapshots.isEmpty() && queryDocumentSnapshots != null)
        {
            mUserCount = queryDocumentSnapshots.size();

            for (QueryDocumentSnapshot snapshot : queryDocumentSnapshots)
            {
                final String id = snapshot.getId();
                final String photoUrl = snapshot.getString("photo");
                final String username = snapshot.getString("username");
                final String email = snapshot.getString("email");
                final String domain = email.substring(email.indexOf("@") + 1, email.lastIndexOf("."));
                Log.i("Log", "Domain: " + domain);

                DocumentReference ref = mRepository.getProgressStatsHighScores(id);
                ref.addSnapshotListener(new EventListener<DocumentSnapshot>()
                {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e)
                    {
                        if (documentSnapshot != null)
                        {
                            long balance = documentSnapshot.getLong("balance");
                            long level = documentSnapshot.getLong("level");

                            HighScoresListItem highScoresListItem = new HighScoresListItem(id, photoUrl, username, domain, balance, level);
                            addHighScoreListItemToList(highScoresListItem);
                        }
                        else if (e != null)
                        {
                            // TODO: Exception Handling
                        }
                    }
                });
            }
        }
    }

    @Override
    public void addHighScoreListItemToList(HighScoresListItem highScoresListItem)
    {
        mHighScoresListItemList.add(highScoresListItem);

        if (mHighScoresListItemList.size() == mUserCount)
        {
            Collections.sort(mHighScoresListItemList, new Comparator<HighScoresListItem>()
            {
                @Override
                public int compare(HighScoresListItem b1, HighScoresListItem b2)
                {
                    return Long.valueOf(b2.getBalance()).compareTo(Long.valueOf(b1.getBalance()));
                }
            });

            Iterator<HighScoresListItem> iterHighScores = mHighScoresListItemList.iterator();
            long counter = 1;
            while (iterHighScores.hasNext())
            {
                iterHighScores.next().setRank(counter);

                counter++;
            }

            setValue(mHighScoresListItemList);
        }
    }
}