package ch.dxc.hackerdy.login;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;

import ch.dxc.hackerdy.R;
import ch.dxc.hackerdy.databinding.ActivityLoginBinding;
import ch.dxc.hackerdy.forgotpassword.ForgotPasswordActivity;
import ch.dxc.hackerdy.home.HomeActivity;
import ch.dxc.hackerdy.login.username.UsernameActivity;
import ch.dxc.hackerdy.registration.RegistrationActivity;
import ch.dxc.hackerdy.util.Regex;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, FacebookCallback<LoginResult>
{
    private static final int RC_REGISTRATION = 1;
    private static final int RC_FORGOT_PASSWORD = 2;
    private static final int RC_GOOGLE_SIGN_IN = 3;

    private ActivityLoginBinding mBinding;
    private LoginViewModel mViewModel;
    private GoogleSignInClient mGoogleClient;
    private CallbackManager mCallbackManager;
    private LoginManager mLoginManager = LoginManager.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        mViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        mBinding.setViewModel(mViewModel);

        mBinding.buttonSignIn.setOnClickListener(this);
        mBinding.buttonForgotPassword.setOnClickListener(this);
        mBinding.buttonSignUp.setOnClickListener(this);
        mBinding.buttonFacebookSignIn.setOnClickListener(this);
        mBinding.buttonGoogleSignIn.setOnClickListener(this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestIdToken(getString(R.string.default_web_client_id)).requestEmail().build();
        mGoogleClient = GoogleSignIn.getClient(this, gso);

        mCallbackManager = CallbackManager.Factory.create();

        mBinding.inputEmail.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void afterTextChanged(Editable editable)
            {
                if (mBinding.inputLayoutEmail.isErrorEnabled())
                {
                    mBinding.inputLayoutEmail.setErrorEnabled(false);
                }
            }
        });

        mBinding.inputPassword.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void afterTextChanged(Editable editable)
            {
                if (mBinding.layoutInputPassword.isErrorEnabled())
                {
                    mBinding.layoutInputPassword.setErrorEnabled(false);
                }
            }
        });

        mViewModel.isLoading().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean loading)
            {
                if (loading)
                {
                    mBinding.progressbar.bringToFront();
                    mBinding.progressbar.setVisibility(View.VISIBLE);
                    mBinding.buttonSignUp.setEnabled(false);
                    mBinding.buttonForgotPassword.setEnabled(false);
                    mBinding.buttonGoogleSignIn.setEnabled(false);
                    mBinding.buttonFacebookSignIn.setEnabled(false);
                    mBinding.inputEmail.setEnabled(false);
                    mBinding.inputPassword.setEnabled(false);
                    mBinding.buttonSignIn.setEnabled(false);
                }
                else
                {
                    mBinding.progressbar.setVisibility(View.GONE);
                    mBinding.buttonSignUp.setEnabled(true);
                    mBinding.buttonForgotPassword.setEnabled(true);
                    mBinding.buttonGoogleSignIn.setEnabled(true);
                    mBinding.buttonFacebookSignIn.setEnabled(true);
                    mBinding.inputEmail.setEnabled(true);
                    mBinding.inputPassword.setEnabled(true);
                    mBinding.buttonSignIn.setEnabled(true);
                }
            }
        });

        mViewModel.getEmailError().observe(this, new Observer<Integer>()
        {
            @Override
            public void onChanged(Integer resId)
            {
                mBinding.inputLayoutEmail.setError(getString(resId));
            }
        });

        mViewModel.getPasswordError().observe(this, new Observer<Integer>()
        {
            @Override
            public void onChanged(Integer resId)
            {
                mBinding.layoutInputPassword.setError(getString(resId));
            }
        });

        mViewModel.getEmailVerified().observe(this, new Observer<FirebaseUser>()
        {
            @Override
            public void onChanged(final FirebaseUser user)
            {
                new AlertDialog.Builder(LoginActivity.this).setTitle(R.string.title_unverified_email).setMessage(R.string.error_unverified_email).setPositiveButton(R.string.button_resend, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        user.sendEmailVerification().addOnSuccessListener(new OnSuccessListener<Void>()
                        {
                            @Override
                            public void onSuccess(Void aVoid)
                            {
                                Snackbar.make(mBinding.activityLogin, R.string.email_verification_link_sended, Snackbar.LENGTH_LONG)
                                        .show();
                            }
                        }).addOnFailureListener(new OnFailureListener()
                        {
                            @Override
                            public void onFailure(@NonNull Exception e)
                            {
                                Snackbar.make(mBinding.activityLogin, R.string.email_verification_link_not_sent, Snackbar.LENGTH_LONG)
                                        .show();
                            }
                        });
                        dialogInterface.dismiss();
                    }
                }).setNegativeButton(R.string.button_close, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        dialogInterface.dismiss();
                    }
                }).create().show();
            }
        });

        mViewModel.getRepositoryError().observe(this, new Observer<String>()
        {
            @Override
            public void onChanged(String msg)
            {
                Snackbar.make(mBinding.activityLogin, msg, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_REGISTRATION)
        {
            if (resultCode == Activity.RESULT_OK)
            {
                mBinding.inputEmail.setText(data.getStringExtra("email"));
                mBinding.inputPassword.setText(data.getStringExtra("password"));

                Snackbar.make(mBinding.activityLogin, R.string.registration_successful, Snackbar.LENGTH_INDEFINITE).setAction(R.string.button_hide, new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                    }
                }).show();
            }
        }
        else if (requestCode == RC_FORGOT_PASSWORD)
        {
            if (resultCode == Activity.RESULT_OK)
            {
                mBinding.inputEmail.setText(data.getStringExtra("email"));

                Snackbar.make(mBinding.activityLogin, R.string.request_password_reset_successful, Snackbar.LENGTH_INDEFINITE).setAction(R.string.button_ok, new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                    }
                }).show();
            }
        }
        else if (requestCode == RC_GOOGLE_SIGN_IN)
        {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try
            {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                mViewModel.signInWithGoogle(account);
            } catch (ApiException e)
            {
                Snackbar.make(mBinding.activityLogin, R.string.google_sign_in_failed, Snackbar.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.button_sign_in:
                mViewModel.signInWithEmailAndPassword();
                break;
            case R.id.button_forgot_password:
                startActivityForResult(new Intent(this, ForgotPasswordActivity.class), RC_FORGOT_PASSWORD);
                break;
            case R.id.button_sign_up:
                startActivityForResult(new Intent(this, RegistrationActivity.class), RC_REGISTRATION);
                break;
            case R.id.button_facebook_sign_in:
                mLoginManager.logInWithReadPermissions(this, Arrays.asList("email"));
                mLoginManager.registerCallback(mCallbackManager, this);
                break;
            case R.id.button_google_sign_in:
                Intent signInIntent = mGoogleClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_GOOGLE_SIGN_IN);
                break;
        }
    }

    @Override
    protected void onStart()
    {
        super.onStart();

        mViewModel.autoLogin();
        mViewModel.getLoginSuccessful().observe(this, new Observer<FirebaseUser>()
        {
            @Override
            public void onChanged(FirebaseUser user)
            {
                String username = user.getDisplayName();

//                user.updateProfile(new UserProfileChangeRequest.Builder().setDisplayName("Stefan Gerber").build());

                if (!username.matches(Regex.DISPLAY_NAME.getRegex()))
                {
                    Intent intent = new Intent(LoginActivity.this, UsernameActivity.class);
                    intent.putExtra("username", username);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                    finish();
                }
            }
        });
    }

    @Override
    public void onSuccess(LoginResult loginResult)
    {
        mViewModel.signInWithFacebook(loginResult.getAccessToken());
    }

    @Override
    public void onCancel()
    {
        Snackbar.make(mBinding.activityLogin, R.string.facebook_sign_in_failed, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onError(FacebookException error)
    {
        Snackbar.make(mBinding.activityLogin, error.getMessage(), Snackbar.LENGTH_LONG).show();
        Log.i("Log", error.getMessage());
    }
}