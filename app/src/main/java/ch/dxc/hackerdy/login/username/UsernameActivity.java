package ch.dxc.hackerdy.login.username;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.material.snackbar.Snackbar;

import ch.dxc.hackerdy.R;
import ch.dxc.hackerdy.databinding.ActivityUsernameBinding;
import ch.dxc.hackerdy.home.HomeActivity;
import ch.dxc.hackerdy.login.LoginActivity;

// TODO: check username existence in Firestore DB (Here onClick or in TextWatcher and in RegistrationActivity onClick or in TextWatcher)
public class UsernameActivity extends AppCompatActivity implements View.OnClickListener
{
    private ActivityUsernameBinding mBinding;
    private UsernameViewModel mViewModel;

    private GoogleSignInClient mGoogleSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_username);

        mViewModel = ViewModelProviders.of(this).get(UsernameViewModel.class);

        mBinding.setViewModel(mViewModel);

        Intent intent = getIntent();
        String username = intent.getExtras().getString("username");
        mViewModel.setUsername(username);

        mBinding.inputUsername.setText(username);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestIdToken(getString(R.string.default_web_client_id)).requestEmail().build();
        mGoogleSignIn = GoogleSignIn.getClient(this, gso);

        mBinding.buttonContinue.setOnClickListener(this);

        mBinding.inputUsername.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void afterTextChanged(Editable editable)
            {
                if (mBinding.layoutInputUsername.isErrorEnabled())
                {
                    mBinding.layoutInputUsername.setErrorEnabled(false);
                    mBinding.layoutInputUsername.setCounterEnabled(true);
                }
            }
        });

        mViewModel.getUsernameError().observe(this, new Observer<Integer>()
        {
            @Override
            public void onChanged(Integer resId)
            {
                mBinding.layoutInputUsername.setError(getString(resId));
                mBinding.layoutInputUsername.setCounterEnabled(false);
            }
        });

        mViewModel.getUpdateSuccessful().observe(this, new Observer<String>()
        {
            @Override
            public void onChanged(String username)
            {
                mViewModel.setLoading(false);

                startActivity(new Intent(UsernameActivity.this, HomeActivity.class));
                finish();
            }
        });

        mViewModel.getRepositoryError().observe(this, new Observer<String>()
        {
            @Override
            public void onChanged(String msg)
            {
                Snackbar.make(mBinding.activityUsername, msg, Snackbar.LENGTH_LONG).show();
                mViewModel.setLoading(false);
            }
        });

        mViewModel.isLoading().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean loading)
            {
                if (loading)
                {
                    mBinding.progressbarOnChangeUsername.setVisibility(View.VISIBLE);
                    mBinding.buttonContinue.setEnabled(false);
                    mBinding.inputUsername.setEnabled(false);
                }
                else
                {
                    mBinding.progressbarOnChangeUsername.setVisibility(View.GONE);
                    mBinding.buttonContinue.setEnabled(true);
                    mBinding.inputUsername.setEnabled(true);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            mViewModel.signOut(mGoogleSignIn);

            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.button_continue:
                mViewModel.onContinue();
        }
    }
}