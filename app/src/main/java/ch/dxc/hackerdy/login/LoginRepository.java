package ch.dxc.hackerdy.login;

import com.facebook.AccessToken;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.firebase.auth.FirebaseUser;

public interface LoginRepository
{

    void signInWithEmailAndPassword(String email, String password, LoginCallback login);

    void updateLoginTimestamp(FirebaseUser user, LoginCallback login);

    FirebaseUser getUser();

    void signInWithGoogle(GoogleSignInAccount account, LoginCallback login);

    void signInWithFacebook(AccessToken token, LoginCallback login);

    void persistUser(FirebaseUser user, LoginCallback login);
}