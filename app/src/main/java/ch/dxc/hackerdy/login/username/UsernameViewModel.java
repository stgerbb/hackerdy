package ch.dxc.hackerdy.login.username;

import androidx.databinding.ObservableField;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.android.gms.auth.api.signin.GoogleSignInClient;

import ch.dxc.hackerdy.R;
import ch.dxc.hackerdy.repositories.FirebaseRepository;
import ch.dxc.hackerdy.settings.UpdateUserAccountDataCallback;
import ch.dxc.hackerdy.util.Regex;

public class UsernameViewModel extends ViewModel implements UpdateUserAccountDataCallback.UpdateCallback, UpdateUserAccountDataCallback.UsernameCallback
{
    public ObservableField<String> mUsername;
    private MutableLiveData<Integer> mUsernameError = new MutableLiveData<>();
    private MutableLiveData<String> mRepositoryError = new MutableLiveData<>();
    private MutableLiveData<String> mUpdateSuccessful = new MutableLiveData<>();
    private MutableLiveData<Boolean> mLoading = new MutableLiveData<>();
    private FirebaseRepository mRepository = new FirebaseRepository();

    public void signOut(GoogleSignInClient googleSignInClient)
    {
        mRepository.signOut(googleSignInClient);
    }

    public void setUsername(String username)
    {
        mUsername = new ObservableField<>(username);

        if (username.isEmpty())
        {
            mUsernameError.setValue(R.string.error_empty_username);
        }
        else if (!username.matches(Regex.DISPLAY_NAME.getRegex()))
        {
            mUsernameError.setValue(R.string.error_username_regex);
        }
    }

    public void onContinue()
    {
        String username = getUsername();

        if (username.isEmpty())
        {
            mUsernameError.setValue(R.string.error_empty_username);
        }
        else if (!username.matches(Regex.DISPLAY_NAME.getRegex()))
        {
            mUsernameError.setValue(R.string.error_username_regex);
        }
        else
        {
            mRepository.changeUsername(username, this, this);
        }
    }

    public LiveData<Integer> getUsernameError()
    {
        return mUsernameError;
    }

    public String getUsername()
    {
        return mUsername.get().trim();
    }

    public void setLoading(Boolean bool)
    {
        mLoading.setValue(bool);
    }

    @Override
    public void onStart()
    {
        setLoading(true);
    }

    @Override
    public void onFailure(String msg)
    {
        mRepositoryError.setValue(msg);
    }

    @Override
    public void reauthenticate(String msg)
    {
        // Not in use
    }

    @Override
    public void onUsernameChangeSuccessful(String username)
    {
        mUpdateSuccessful.setValue(username);
    }

    @Override
    public void usernameAlreadyExists()
    {
        mUsernameError.setValue(R.string.error_username_already_in_use);
    }

    public LiveData<Boolean> isLoading()
    {
        return mLoading;
    }

    public LiveData<String> getRepositoryError()
    {
        return mRepositoryError;
    }

    public LiveData<String> getUpdateSuccessful()
    {
        return mUpdateSuccessful;
    }
}