package ch.dxc.hackerdy.settings;

import android.util.Patterns;

import androidx.databinding.ObservableField;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.firebase.auth.FirebaseUser;

import ch.dxc.hackerdy.R;
import ch.dxc.hackerdy.repositories.FirebaseRepository;
import ch.dxc.hackerdy.util.Regex;

public class SettingsViewModel extends ViewModel implements UpdateUserAccountDataCallback.UpdateCallback, UpdateUserAccountDataCallback.UsernameCallback, UpdateUserAccountDataCallback.EmailCallback, UpdateUserAccountDataCallback.DeleteAccountCallback, UpdateUserAccountDataCallback.PasswordCallback
{
    private FirebaseRepository mRepository = new FirebaseRepository();
    public final ObservableField<String> mUsername;
    public final ObservableField<String> mEmail;
    public final ObservableField<String> mPassword = new ObservableField<>("");
    public final ObservableField<String> mConfirmPassword = new ObservableField<>("");
    private MutableLiveData<Integer> mConfirmPasswordError = new MutableLiveData<>();
    private MutableLiveData<Integer> mPasswordError = new MutableLiveData<>();
    private MutableLiveData<Integer> mEmailError = new MutableLiveData<>();
    private MutableLiveData<Integer> mUsernameError = new MutableLiveData<>();
    private MutableLiveData<Boolean> mLoading = new MutableLiveData<>();
    private MutableLiveData<String> mRepositoryError = new MutableLiveData<>();
    private MutableLiveData<String> mEmailUpdate = new MutableLiveData<>();
    private MutableLiveData<Boolean> mPasswordUpdate = new MutableLiveData<>();
    private MutableLiveData<String> mUsernameUpdate = new MutableLiveData<>();
    private MutableLiveData<Boolean> mAccountDeleted = new MutableLiveData<>();
    private MutableLiveData<String> mReauthenticate = new MutableLiveData<>();
    private MutableLiveData<Boolean> mFederatedIdentityLogin = new MutableLiveData<>();
    private String currentEmail;
    private String currentUsername;

    public SettingsViewModel()
    {
        FirebaseUser user = mRepository.getUser();
        mUsername = new ObservableField<>(user.getDisplayName());
        mEmail = new ObservableField<>(user.getEmail());
        currentEmail = getEmail();
        currentUsername = getUsername();
        mFederatedIdentityLogin.setValue(mRepository.isFederatedIdentityLogin());
    }

    private String getEmail()
    {
        return mEmail.get().trim();
    }

    private String getUsername()
    {
        return mUsername.get().trim();
    }

    public void changeEmail()
    {
        String email = getEmail();

        if (email.isEmpty())
        {
            mEmailError.setValue(R.string.error_empty_email);
        }
        else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches())
        {
            mEmailError.setValue(R.string.error_email_format);
        }
        else if (currentEmail.equals(email))
        {
            return;
        }
        else
        {
            mRepository.changeEmail(email, this, this);
        }
    }

    public void changeUsername()
    {
        String username = getUsername();

        if (username.isEmpty())
        {
            mUsernameError.setValue(R.string.error_empty_username);
        }
        else if (!username.matches(Regex.DISPLAY_NAME.getRegex()))
        {
            mUsernameError.setValue(R.string.error_username_regex);
        }
        else if (currentUsername.equals(username))
        {
            return;
        }
        else
        {
            mRepository.changeUsername(username, this, this);
        }
    }

    public void changePassword()
    {
        String password = mPassword.get();
        String confirmPassword = mConfirmPassword.get();

        if (password.isEmpty())
        {
            mPasswordError.setValue(R.string.error_empty_password);
        }
        else if (confirmPassword.isEmpty())
        {
            mConfirmPasswordError.setValue(R.string.error_empty_password);
        }
        else if (password.matches(Regex.PASSWORD.getRegex()))
        {
            mPasswordError.setValue(R.string.error_password_regex);
        }
        else if (!password.equals(confirmPassword) || !confirmPassword.equals(password))
        {
            mConfirmPasswordError.setValue(R.string.error_passwords_do_not_match);
        }
        else
        {
            mRepository.changePassword(password, this, this);
        }
    }

    public LiveData<Boolean> isFederatedIdentityLogin()
    {
        return mFederatedIdentityLogin;
    }

    public void deleteAccount()
    {
        mRepository.deleteAccount(this, this);
    }

    public LiveData<Boolean> isAccountDeleted()
    {
        return mAccountDeleted;
    }

    public LiveData<String> getUsernameUpdate()
    {
        return mUsernameUpdate;
    }

    public LiveData<String> getEmailUpdate()
    {
        return mEmailUpdate;
    }

    public LiveData<Boolean> getPasswordUpdate()
    {
        return mPasswordUpdate;
    }

    public LiveData<String> getRepositoryError()
    {
        return mRepositoryError;
    }

    public LiveData<Boolean> isLoading()
    {
        return mLoading;
    }

    public LiveData<Integer> getUsernameError()
    {
        return mUsernameError;
    }

    public LiveData<Integer> getEmailError()
    {
        return mEmailError;
    }

    public LiveData<Integer> getPasswordError()
    {
        return mPasswordError;
    }

    public LiveData<Integer> getConfirmPasswordError()
    {
        return mConfirmPasswordError;
    }

    public LiveData<String> getReauthenticate()
    {
        return mReauthenticate;
    }

    public void setLoading(Boolean bool)
    {
        mLoading.setValue(bool);
    }

    public void signOut(GoogleSignInClient googleClient)
    {
        mRepository.signOut(googleClient);
    }

    @Override
    public void onUsernameChangeSuccessful(String username)
    {
        mUsernameUpdate.setValue(username);
    }

    @Override
    public void usernameAlreadyExists()
    {
        setLoading(false);
        mUsernameError.setValue(R.string.error_username_already_in_use);
    }

    @Override
    public void onStart()
    {
        setLoading(true);
    }

    @Override
    public void onFailure(String msg)
    {
        mRepositoryError.setValue(msg);
    }

    @Override
    public void reauthenticate(String msg)
    {
        mReauthenticate.setValue(msg);
    }

    @Override
    public void onEmailChangeSuccessful(String email)
    {
        mEmailUpdate.setValue(email);
    }

    @Override
    public void onAccountDeleted()
    {
        mAccountDeleted.setValue(true);
    }

    @Override
    public void onPasswordChangeSuccessful()
    {
        mPasswordUpdate.setValue(true);
    }
}