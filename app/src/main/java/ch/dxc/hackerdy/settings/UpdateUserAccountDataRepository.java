package ch.dxc.hackerdy.settings;

public interface UpdateUserAccountDataRepository
{
    void changePassword(String password, UpdateUserAccountDataCallback.UpdateCallback update, UpdateUserAccountDataCallback.PasswordCallback callback);

    void changeUsername(String username, UpdateUserAccountDataCallback.UpdateCallback update, UpdateUserAccountDataCallback.UsernameCallback callback);

    void changeEmail(String email, UpdateUserAccountDataCallback.UpdateCallback update, UpdateUserAccountDataCallback.EmailCallback callback);

    void deleteAccount(UpdateUserAccountDataCallback.UpdateCallback update, UpdateUserAccountDataCallback.DeleteAccountCallback delete);

    Boolean isFederatedIdentityLogin();
}