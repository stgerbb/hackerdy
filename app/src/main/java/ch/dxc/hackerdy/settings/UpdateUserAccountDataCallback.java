package ch.dxc.hackerdy.settings;

public interface UpdateUserAccountDataCallback
{
    interface UsernameCallback
    {
        void onUsernameChangeSuccessful(String username);

        void usernameAlreadyExists();
    }

    interface UpdateCallback
    {
        void onStart();

        void onFailure(String msg);

        void reauthenticate(String msg);
    }

    interface EmailCallback
    {
        void onEmailChangeSuccessful(String email);
    }

    interface DeleteAccountCallback
    {
        void onAccountDeleted();
    }

    interface PasswordCallback
    {
        void onPasswordChangeSuccessful();
    }
}