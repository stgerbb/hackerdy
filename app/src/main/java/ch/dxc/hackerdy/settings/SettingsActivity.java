package ch.dxc.hackerdy.settings;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.material.snackbar.Snackbar;

import ch.dxc.hackerdy.R;
import ch.dxc.hackerdy.databinding.ActivitySettingsBinding;
import ch.dxc.hackerdy.login.LoginActivity;
import ch.dxc.hackerdy.settings.language.ChangeLanguageActivity;
import ch.dxc.hackerdy.util.PasswordUtil;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener
{

    private ActivitySettingsBinding mBinding;
    private SettingsViewModel mViewModel;
    private PasswordUtil passwordUtil = new PasswordUtil();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_settings);

        mViewModel = ViewModelProviders.of(this).get(SettingsViewModel.class);
        mBinding.setViewModel(mViewModel);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mBinding.buttonChangeEmailAddress.setOnClickListener(this);
        mBinding.buttonChangePassword.setOnClickListener(this);
        mBinding.buttonChangeUsername.setOnClickListener(this);
        mBinding.buttonDeleteAccount.setOnClickListener(this);
        mBinding.buttonChangeLanguage.setOnClickListener(this);

        mBinding.inputUsername.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void afterTextChanged(Editable editable)
            {
                if (mBinding.inputLayoutUsername.isErrorEnabled())
                {
                    mBinding.inputLayoutUsername.setErrorEnabled(false);
                    mBinding.inputLayoutUsername.setCounterEnabled(true);

                }
            }
        });

        mBinding.inputEmail.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void afterTextChanged(Editable editable)
            {
                if (mBinding.inputLayoutEmail.isErrorEnabled())
                {
                    mBinding.inputLayoutEmail.setErrorEnabled(false);
                }
            }
        });

        mBinding.inputNewPassword.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void afterTextChanged(Editable editable)
            {
                if (mBinding.inputLayoutNewPassword.isErrorEnabled())
                {
                    mBinding.inputLayoutNewPassword.setErrorEnabled(false);
                }

                passwordUtil.showPasswordStrengthMeter(mBinding.textPasswordStrength, mBinding.progressbarPasswordStrength, editable);
                mBinding.textPasswordStrength.setText(getString(passwordUtil.getStringId()));
                mBinding.progressbarPasswordStrength.setProgress(passwordUtil.getProgress());
            }
        });

        mBinding.inputConfirmNewPassword.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void afterTextChanged(Editable editable)
            {
                if (mBinding.inputLayoutConfirmNewPassword.isErrorEnabled())
                {
                    mBinding.inputLayoutConfirmNewPassword.setErrorEnabled(false);
                }
            }
        });

        mViewModel.isFederatedIdentityLogin().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean federatedIdentityLogin)
            {
                if (federatedIdentityLogin)
                {
                    mBinding.inputLayoutEmail.setVisibility(View.GONE);
                    mBinding.buttonChangeEmailAddress.setVisibility(View.GONE);
                    mBinding.inputLayoutNewPassword.setVisibility(View.GONE);
                    mBinding.inputLayoutConfirmNewPassword.setVisibility(View.GONE);
                    mBinding.buttonChangePassword.setVisibility(View.GONE);
                }
            }
        });

        mViewModel.isAccountDeleted().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean accountDeleted)
            {
                if (accountDeleted)
                {
                    mViewModel.setLoading(false);

                    startActivityLogin();
                }
            }
        });

        mViewModel.getUsernameUpdate().observe(this, new Observer<String>()
        {
            @Override
            public void onChanged(String username)
            {
                Snackbar.make(mBinding.activitySettings, String.format("%s %s", getString(R.string.text_username_change_successful), username), Snackbar.LENGTH_INDEFINITE).setAction(R.string.button_hide, new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                    }
                }).show();

                mViewModel.setLoading(false);

                mBinding.inputUsername.setEnabled(false);
                mBinding.buttonChangeUsername.setEnabled(false);
            }
        });

        mViewModel.getReauthenticate().observe(this, new Observer<String>()
        {
            @Override
            public void onChanged(String error)
            {
                mViewModel.setLoading(false);

                new AlertDialog.Builder(SettingsActivity.this).setTitle(R.string.title_information).setMessage(error).setPositiveButton(R.string.button_reauthenticate, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        signOut();
                    }
                }).setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        dialogInterface.dismiss();
                    }
                }).create().show();
            }
        });

        mViewModel.getEmailUpdate().observe(this, new Observer<String>()
        {
            @Override
            public void onChanged(String email)
            {
                new AlertDialog.Builder(SettingsActivity.this).setTitle(R.string.title_information).setMessage(String.format("%s %s. %s", getString(R.string.text_email_change_successful), email, getString(R.string.text_email_verification_link_sent))).setPositiveButton(R.string.button_close, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        dialogInterface.dismiss();
                    }
                }).create().show();

                mViewModel.setLoading(false);

                mBinding.inputEmail.setEnabled(false);
                mBinding.buttonChangeEmailAddress.setEnabled(false);
            }
        });

        mViewModel.getPasswordUpdate().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean updateSuccessful)
            {
                Snackbar.make(mBinding.activitySettings, getString(R.string.text_password_change_successful), Snackbar.LENGTH_INDEFINITE).setAction(R.string.button_hide, new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                    }
                }).show();

                mViewModel.setLoading(false);

                mBinding.inputNewPassword.setEnabled(false);
                mBinding.inputConfirmNewPassword.setEnabled(false);
                mBinding.buttonChangePassword.setEnabled(false);
            }
        });

        mViewModel.getRepositoryError().observe(this, new Observer<String>()
        {
            @Override
            public void onChanged(String error)
            {
                mViewModel.setLoading(false);

                new AlertDialog.Builder(SettingsActivity.this).setTitle(R.string.title_information).setMessage(error).setPositiveButton(R.string.button_close, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        dialogInterface.dismiss();
                    }
                }).create().show();
            }
        });

        mViewModel.isLoading().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean loading)
            {
                if (loading)
                {
                    mBinding.progressbarOnChange.bringToFront();
                    mBinding.progressbarOnChange.setVisibility(View.VISIBLE);
                    mBinding.inputUsername.setEnabled(false);
                    mBinding.buttonChangeUsername.setEnabled(false);
                    mBinding.inputEmail.setEnabled(false);
                    mBinding.buttonChangeEmailAddress.setEnabled(false);
                    mBinding.inputNewPassword.setEnabled(false);
                    mBinding.inputConfirmNewPassword.setEnabled(false);
                    mBinding.buttonChangePassword.setEnabled(false);
                    mBinding.buttonChangeLanguage.setEnabled(false);
                    mBinding.buttonDeleteAccount.setEnabled(false);
                }
                else
                {
                    mBinding.progressbarOnChange.setVisibility(View.GONE);
                    mBinding.inputUsername.setEnabled(true);
                    mBinding.buttonChangeUsername.setEnabled(true);
                    mBinding.inputEmail.setEnabled(true);
                    mBinding.buttonChangeEmailAddress.setEnabled(true);
                    mBinding.inputNewPassword.setEnabled(true);
                    mBinding.inputConfirmNewPassword.setEnabled(true);
                    mBinding.buttonChangePassword.setEnabled(true);
                    mBinding.buttonChangeLanguage.setEnabled(true);
                    mBinding.buttonDeleteAccount.setEnabled(true);
                }
            }
        });

        mViewModel.getUsernameError().observe(this, new Observer<Integer>()
        {
            @Override
            public void onChanged(Integer resId)
            {
                mBinding.inputLayoutUsername.setError(getString(resId));
                mBinding.inputLayoutUsername.setCounterEnabled(false);
            }
        });

        mViewModel.getEmailError().observe(this, new Observer<Integer>()
        {
            @Override
            public void onChanged(Integer resId)
            {
                mBinding.inputLayoutEmail.setError(getString(resId));
            }
        });

        mViewModel.getPasswordError().observe(this, new Observer<Integer>()
        {
            @Override
            public void onChanged(Integer resId)
            {
                mBinding.inputLayoutNewPassword.setError(getString(resId));
            }
        });

        mViewModel.getConfirmPasswordError().observe(this, new Observer<Integer>()
        {
            @Override
            public void onChanged(Integer resId)
            {
                mBinding.inputLayoutConfirmNewPassword.setError(getString(resId));
            }
        });
    }

    private void signOut()
    {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestIdToken(getString(R.string.default_web_client_id)).requestEmail().build();
        GoogleSignInClient googleClient = GoogleSignIn.getClient(SettingsActivity.this, gso);

        mViewModel.signOut(googleClient);

        startActivityLogin();
    }

    private void startActivityLogin()
    {
        Intent login = new Intent(this, LoginActivity.class);
        login.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(login);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void showDeleteAccountAlertDialog()
    {
        new AlertDialog.Builder(this).setTitle(R.string.title_account_deletion).setMessage(R.string.warning_account_deletion).setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                dialogInterface.dismiss();
            }
        }).setPositiveButton(R.string.button_delete, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                mViewModel.deleteAccount();
            }
        }).create().show();
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.button_change_email_address:
                mViewModel.changeEmail();
                break;
            case R.id.button_change_password:
                mViewModel.changePassword();
                break;
            case R.id.button_change_username:
                mViewModel.changeUsername();
                break;
            case R.id.button_change_language:
                startActivity(new Intent(this, ChangeLanguageActivity.class));
                break;
            case R.id.button_delete_account:
                showDeleteAccountAlertDialog();
                break;
        }
    }
}