package ch.dxc.hackerdy.questionnaire;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.util.ArrayList;
import java.util.List;

import ch.dxc.hackerdy.R;
import ch.dxc.hackerdy.databinding.ActivityQuestionnaireBinding;
import ch.dxc.hackerdy.quiz.QuizActivity;

public class QuestionnaireActivity extends AppCompatActivity
{
    private ActivityQuestionnaireBinding mBinding;
    private QuestionnaireViewModel mViewModel;
    private String mMode;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_questionnaire);
        mViewModel = ViewModelProviders.of(this).get(QuestionnaireViewModel.class);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mViewModel.getTopics().observe(this, new Observer<List<TopicListItem>>()
        {
            @Override
            public void onChanged(final List<TopicListItem> topicList)
            {
                mViewModel.queryPlayedTopics();
                mViewModel.getPlayedTopics().observe(QuestionnaireActivity.this, new Observer<List<String>>()
                {
                    @Override
                    public void onChanged(List<String> playedTopicsList)
                    {
                        List<TopicListItem> filteredList = mViewModel.filterList(topicList, playedTopicsList);

                        switch (filteredList.size())
                        {
                            case 9:
                                setStageText("1 / 3");
                                break;
                            case 6:
                                setStageText("2 / 3");
                                break;
                            case 3:
                                setStageText("3 / 3");
                                break;
                        }

                        TopicsListAdapter adapter = new TopicsListAdapter(QuestionnaireActivity.this, R.layout.list_item_topic, filteredList);
                        mBinding.listTopics.setAdapter(adapter);

                        mViewModel.setLoading(false);
                    }
                });
            }
        });

        mViewModel.isLoading().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean loading)
            {
                if (loading)
                {
                    mBinding.progressbarTopics.setVisibility(View.VISIBLE);
                    mBinding.listTopics.setVisibility(View.GONE);
                }
                else
                {
                    mBinding.progressbarTopics.setVisibility(View.GONE);
                    mBinding.listTopics.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.info_menu, menu);

        return true;
    }

    private void showTutorialAlertDialog()
    {
        new AlertDialog.Builder(this).setTitle(R.string.title_tutorial).setMessage(R.string.text_tutorial_questionnaire).setPositiveButton(R.string.button_hide, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    private void setStageText(String stage)
    {
        mBinding.textStage.setText(String.format("%s %s", getString(R.string.text_stage), stage));
    }

    private class TopicsListAdapter extends ArrayAdapter<TopicListItem>
    {
        private ArrayList<String> mSelectedListItemList = new ArrayList<>();
        private int mResource;

        public TopicsListAdapter(@NonNull Context context, int resource, @NonNull List<TopicListItem> objects)
        {
            super(context, resource, objects);

            mResource = resource;
        }

        // TODO - Bug: Checkbox gets unchecked when scrolling the ListView
        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent)
        {
            ViewHolder holder;

            if (convertView == null)
            {
                LayoutInflater inflater = (LayoutInflater) getApplication().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(mResource, parent, false);

                holder = new ViewHolder();

                holder.mCheckBox = convertView.findViewById(R.id.cb_topic);
                holder.mImage = convertView.findViewById(R.id.image_info_to_topic);

                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.mCheckBox.setText(getItem(position).getName());
            holder.mCheckBox.setChecked(getItem(position).isChecked());

            holder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
            {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean checked)
                {
                    if (checked)
                    {
                        getItem(position).setChecked(true);
                        mSelectedListItemList.add(getItem(position).getName());

                        if (mSelectedListItemList.size() == 3)
                        {
                            Intent quiz = new Intent(QuestionnaireActivity.this, QuizActivity.class);
                            quiz.putStringArrayListExtra("topics", mSelectedListItemList);
                            startActivity(quiz);
                        }
                    }
                    else
                    {
                        getItem(position).setChecked(false);

                        mSelectedListItemList.remove(getItem(position).getName());
                    }
                }

            });

            holder.mImage.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    startActivity(new Intent(QuestionnaireActivity.this, TopicActivity.class));
                }
            });

            return convertView;
        }

        private class ViewHolder
        {
            CheckBox mCheckBox;
            ImageView mImage;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                break;
            case R.id.info_menu:
                showTutorialAlertDialog();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}