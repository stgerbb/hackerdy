package ch.dxc.hackerdy.questionnaire;

public class TopicListItem
{
    private String mName;
    private boolean mChecked = false;

    public TopicListItem(String name)
    {
        mName = name;
    }

    public String getName()
    {
        return mName;
    }

    public void setName(String name)
    {
        mName = name;
    }

    public void setChecked(boolean bool)
    {
        mChecked = bool;
    }

    public boolean isChecked()
    {
        return mChecked;
    }
}