package ch.dxc.hackerdy.questionnaire;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

import ch.dxc.hackerdy.repositories.FirebaseRepository;

public class QuestionnaireViewModel extends ViewModel implements PlayedTopicsListener
{
    private MutableLiveData<Boolean> mLoading = new MutableLiveData<>();
    private MutableLiveData<List<String>> mPlayedTopics = new MutableLiveData<>();
    private FirebaseRepository mRepository = new FirebaseRepository();

    public LiveData<List<TopicListItem>> getTopics()
    {
        mLoading.setValue(true);

        return mRepository.getTopics();
    }

    LiveData<Boolean> isLoading()
    {
        return mLoading;
    }

    public void setLoading(boolean bool)
    {
        mLoading.setValue(bool);
    }

    public void queryPlayedTopics()
    {
        mRepository.getPlayedTopics(this);
    }

    public LiveData<List<String>> getPlayedTopics()
    {
         return mPlayedTopics;
    }

    public List<TopicListItem> filterList(List<TopicListItem> topicList, List<String> playedTopicsList)
    {
        List<TopicListItem> filteredList = new ArrayList<>();

        for (TopicListItem topic : topicList)
        {
            if (!playedTopicsList.contains(topic.getName()))
            {
                filteredList.add(topic);
            }
        }

        return filteredList;
    }

    @Override
    public void returnPlayedTopics(List<String> list)
    {
        mPlayedTopics.setValue(list);
    }
}