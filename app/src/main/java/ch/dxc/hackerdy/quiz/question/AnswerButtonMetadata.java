package ch.dxc.hackerdy.quiz.question;

public class AnswerButtonMetadata
{
    private boolean mCorrect;
    private String mReason;
    private String mSolution;
    private byte[] mImage;

    public boolean isCorrect()
    {
        return mCorrect;
    }

    public void setCorrect(boolean bool)
    {
        mCorrect = bool;
    }

    public void setSolution(String solution)
    {
        mSolution = solution;
    }

    public String getSolution()
    {
        return mSolution;
    }

    public void setReason(String reason)
    {
        mReason = reason;
    }

    public String getReason()
    {
        return mReason;
    }

    public void setImage(byte[] image)
    {
        mImage = image;
    }

    public byte[] getImage()
    {
        return mImage;
    }
}