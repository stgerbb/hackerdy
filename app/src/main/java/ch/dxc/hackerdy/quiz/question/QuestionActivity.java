package ch.dxc.hackerdy.quiz.question;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import ch.dxc.hackerdy.R;
import ch.dxc.hackerdy.databinding.ActivityQuestionBinding;

public class QuestionActivity extends AppCompatActivity
{
    private ActivityQuestionBinding mBinding;
    private long mPoints;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_question);

        Intent data = getIntent();
        mPoints = data.getLongExtra("points", 0);

        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right)
                .replace(R.id.fragment_container_question, new QuestionFragment())
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onBackPressed()
    {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container_question);

        if (fragment instanceof QuestionFragment)
        {
            new AlertDialog.Builder(this).setTitle(R.string.title_leave_question).setMessage(String.format("%s %s points %s", getString(R.string.text_leave_question), String.valueOf(mPoints), getString(R.string.text_leave_question_2))).setPositiveButton(R.string.button_leave, new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialogInterface, int i)
                {
                    setResult(Activity.RESULT_OK, new Intent().putExtra("score", mPoints * -1));

                    finish();
                }
            }).setNegativeButton(R.string.button_stay, new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialogInterface, int i)
                {
                    dialogInterface.dismiss();
                }
            }).create().show();
        }
        else if (fragment instanceof CorrectAnswerFragment)
        {
            Intent data = new Intent();
            data.putExtra("score", mPoints);
            data.putExtra("correct", true);

            setResult(Activity.RESULT_OK, data);

            finish();
        }
        else if (fragment instanceof WrongAnswerFragment)
        {
            Intent data = new Intent();
            data.putExtra("score", mPoints * -1);
            data.putExtra("correct", false);

            setResult(Activity.RESULT_OK, data);

            finish();
        }
    }
}