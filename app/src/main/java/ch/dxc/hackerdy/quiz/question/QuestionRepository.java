package ch.dxc.hackerdy.quiz.question;

import androidx.lifecycle.LiveData;

public interface QuestionRepository
{
    LiveData<Question> getQuestion(String topic, long points);
}