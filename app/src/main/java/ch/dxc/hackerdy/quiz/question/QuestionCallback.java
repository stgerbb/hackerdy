package ch.dxc.hackerdy.quiz.question;

public interface QuestionCallback
{
    void onSuccess(Question question);

    void onFailure(String msg);
}