package ch.dxc.hackerdy.quiz.result;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.snackbar.Snackbar;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ch.dxc.hackerdy.R;
import ch.dxc.hackerdy.databinding.ActivityResultBinding;
import ch.dxc.hackerdy.home.HomeActivity;

public class ResultActivity extends AppCompatActivity implements View.OnClickListener
{
    private static final long MAX_SCORE = 4500;
    private static final int TOTAL_NUMBER_OF_QUESTIONS = 9;
    private static final int MAX_TIME = 302000;

    private ActivityResultBinding mBinding;
    private ResultViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_result);

        mViewModel = ViewModelProviders.of(this).get(ResultViewModel.class);

        mBinding.buttonNewQuiz.setOnClickListener(this);
        mBinding.buttonMainMenu.setOnClickListener(this);

        Intent data = getIntent();
        int totalAnsweredCorrectCount = data.getIntExtra("total_answered_correct_count", 0);
        final long score = data.getLongExtra("score", 1000);
        final String time = data.getStringExtra("time");
        final int seconds = data.getIntExtra("seconds", 0);
        final int minutes = data.getIntExtra("minutes", 0);
        List<String> playedTopicsList = data.getStringArrayListExtra("played_topics");

        Map<String, Integer> answeredCorrectWrongStats = new HashMap<>();
        answeredCorrectWrongStats.put("answered_correct", totalAnsweredCorrectCount);
        answeredCorrectWrongStats.put("answered_wrong", data.getIntExtra("total_answered_wrong_count", 0));

        Map<String, Integer> topic1Stats = new HashMap<>();
        topic1Stats.put("answered_correct", data.getIntExtra("topic1_answered_correct_count", 0));
        topic1Stats.put("answered_wrong", data.getIntExtra("topic1_answered_wrong_count", 0));

        Map<String, Integer> topic2Stats = new HashMap<>();
        topic2Stats.put("answered_correct", data.getIntExtra("topic2_answered_correct_count", 0));
        topic2Stats.put("answered_wrong", data.getIntExtra("topic2_answered_wrong_count", 0));

        Map<String, Integer> topic3Stats = new HashMap<>();
        topic3Stats.put("answered_correct", data.getIntExtra("topic3_answered_correct_count", 0));
        topic3Stats.put("answered_wrong", data.getIntExtra("topic3_answered_wrong_count", 0));

        mBinding.progressbarNumberCorrectAnswers.setProgress((totalAnsweredCorrectCount * 100) / TOTAL_NUMBER_OF_QUESTIONS);
        mBinding.textNumberCorrectAnswers.setText(String.format("%s %s %s", String.valueOf(totalAnsweredCorrectCount), getString(R.string.text_of), String.valueOf(TOTAL_NUMBER_OF_QUESTIONS)));

        mBinding.progressbarScore.setProgress((int) ((score * 100) / MAX_SCORE));
        mBinding.textScore.setText(String.format("%s %s %s", String.valueOf(score), getString(R.string.text_of), String.valueOf(MAX_SCORE)));

        int milliseconds = secondsToMilliseconds(seconds) + minutesToMilliseconds(minutes);
        mBinding.progressbarTime.setProgress((milliseconds * 100) / MAX_TIME);
        mBinding.textTime.setText(String.format("%s %s %s", time, getString(R.string.text_of), getString(R.string.text_max_game_time)));

        // TODO to implement
//        getSupportFragmentManager().beginTransaction()
//                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right)
//                .replace(R.id.fragment_container_result_2, new TrophiesUnlockedFragment())
//                .addToBackStack(null)
//                .commit();

        // score, playedTopicsList

        mViewModel.getPersonalBest().observe(this, new Observer<Map<String, Object>>()
        {
            @Override
            public void onChanged(Map<String, Object> personalBestMap)
            {
                if (personalBestMap.size() != 0)
                {
                    long scoreBestRun = (long) personalBestMap.get("score");
                    String[] timeBestRunArray = ((String) personalBestMap.get("time")).split(":");
                    int minutesBestRun = Integer.parseInt(timeBestRunArray[0]);
                    int secondsBestRun = Integer.parseInt(timeBestRunArray[1]);

                    if (score == scoreBestRun && minutes < minutesBestRun && seconds < secondsBestRun)
                    {
                        mViewModel.updatePersonalBest(score, time);
                    }
                    else if (score > scoreBestRun)
                    {
                        mViewModel.updatePersonalBest(score, time);
                    }
                }
                else // No personal best until now
                {
                    if (score > 0)
                    {
                        mViewModel.updatePersonalBest(score, time);
                    }
                }
            }
        });

        // TODO: To improve with MediatorLiveData
        mViewModel.getProgressStats().observe(this, new Observer<Map<String, Object>>()
        {
            @Override
            public void onChanged(Map<String, Object> progressStatsMap)
            {
                final long balance = (long) progressStatsMap.get("balance");
                final long currentLevel = (long) progressStatsMap.get("level");
                final long nextLevel = currentLevel + 1;

                mViewModel.getCurrentLevelData(currentLevel).observe(ResultActivity.this, new Observer<Map<String, Long>>()
                {
                    @Override
                    public void onChanged(Map<String, Long> currentLevelDataMap)
                    {
                        final long currentLevelPoints = currentLevelDataMap.get("points");

                        mViewModel.getNextLevelData(nextLevel).observe(ResultActivity.this, new Observer<Map<String, Long>>()
                        {
                            @Override
                            public void onChanged(Map<String, Long> nextLevelDataMap)
                            {
                                long nextLevelPoints = nextLevelDataMap.get("points");

                                mBinding.textCurrentLevel.setText(String.valueOf(currentLevel));
                                mBinding.textNextLevel.setText(String.valueOf(nextLevel));

                                mBinding.textLevelProgress.setText(String.format("%s %s %s %s", String.valueOf(balance), getString(R.string.text_of), String.valueOf(nextLevelPoints), getString(R.string.text_points)));

                                long pointsNeeded = nextLevelPoints - currentLevelPoints;
                                long calc1 = nextLevelPoints - balance;
                                long calc2 = pointsNeeded - calc1;

                                mBinding.progressbarLevelProgress.setProgress((int) ((calc2 * 100) / pointsNeeded));

                                if (!mViewModel.isLevelUp() && balance >= nextLevelPoints)
                                {
                                    mViewModel.updateLevel(nextLevel); // An update is going to happen
                                }
                                else
                                {
                                    if (!mViewModel.isLevelUp() && !mViewModel.isNewPersonalBest() && mViewModel.getUpdateCount() == 1)
                                    {
                                        showNoAchievementFragment();

                                        mViewModel.incUpdateCount();
                                    }
                                    else if (mViewModel.getUpdateCount() == 0 && score == 0) // When score is 0, no update is going to happen
                                    {
                                        showNoAchievementFragment();

                                        mViewModel.incUpdateCount();
                                    }
                                    else if (mViewModel.getUpdateCount() == 0 && score > 0)
                                    {
                                        mViewModel.incUpdateCount(); // First iteration is always no update, but by calling updateStats(), the seconds iteration starts, which is an update. Same by calling updateLevel()
                                    }
                                }

                                mViewModel.setLoading(false);
                            }
                        });
                    }
                });
            }
        });

        mViewModel.isLoading().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean loading)
            {
                if (loading)
                {
                    mBinding.progressbarOnProgressLoad.setVisibility(View.VISIBLE);
                    mBinding.textTitleCurrentLevel.setVisibility(View.GONE);
                    mBinding.textTitleNextLevel.setVisibility(View.GONE);
                    mBinding.progressbarLevelProgress.setVisibility(View.GONE);
                    mBinding.textNextLevel.setVisibility(View.GONE);
                    mBinding.textCurrentLevel.setVisibility(View.GONE);
                    mBinding.textLevelProgress.setVisibility(View.GONE);
                }
                else
                {
                    mBinding.progressbarOnProgressLoad.setVisibility(View.GONE);
                    mBinding.textTitleCurrentLevel.setVisibility(View.VISIBLE);
                    mBinding.textTitleNextLevel.setVisibility(View.VISIBLE);
                    mBinding.progressbarLevelProgress.setVisibility(View.VISIBLE);
                    mBinding.textNextLevel.setVisibility(View.VISIBLE);
                    mBinding.textCurrentLevel.setVisibility(View.VISIBLE);
                    mBinding.textLevelProgress.setVisibility(View.VISIBLE);
                }
            }
        });

        mViewModel.getRepositoryError().observe(this, new Observer<String>()
        {
            @Override
            public void onChanged(String msg)
            {
                Snackbar.make(mBinding.activityResult, msg, Snackbar.LENGTH_LONG).show();
            }
        });

        mViewModel.getLevelUp().observe(this, new Observer<Long>()
        {
            @Override
            public void onChanged(Long nextLevel)
            {
                getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right)
                        .replace(R.id.fragment_container_result_1, LevelUpFragment.newInstance(nextLevel))
                        .addToBackStack(null)
                        .commit();
            }
        });

        mViewModel.getNewPersonalBest().observe(this, new Observer<Map<String, Object>>()
        {
            @Override
            public void onChanged(Map<String, Object> newPersonalBestMap)
            {
                long score = (long) newPersonalBestMap.get("score");
                String time = (String) newPersonalBestMap.get("time");
                if (mViewModel.isLevelUp())
                {
                    getSupportFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right)
                            .replace(R.id.fragment_container_result_2, NewPersonalBestFragment.getInstance(score, time))
                            .addToBackStack(null)
                            .commit();
                }
                else
                {
                    getSupportFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right)
                            .replace(R.id.fragment_container_result_1, NewPersonalBestFragment.getInstance(score, time))
                            .addToBackStack(null)
                            .commit();
                }
            }
        });

        mViewModel.updateStats(score,
                playedTopicsList,
                answeredCorrectWrongStats,
                topic1Stats,
                topic2Stats,
                topic3Stats,
                time);
    }

    @Override
    public void onBackPressed()
    {
        Intent home = new Intent(ResultActivity.this, HomeActivity.class);
        home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(home);
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.button_new_quiz:
                finish();
                break;
            case R.id.button_main_menu:
                onBackPressed();
                break;
        }
    }

    private int secondsToMilliseconds(int seconds)
    {
        return seconds * 1000;
    }

    private int minutesToMilliseconds(int minutes)
    {
        int milliseconds = 0;
        switch (minutes)
        {
            case 1:
                milliseconds = 60000;
                break;
            case 2:
                milliseconds = 120000;
                break;
            case 3:
                milliseconds = 180000;
                break;
            case 4:
                milliseconds = 240000;
                break;
            case 5:
                milliseconds = 300000;
                break;
        }
        return milliseconds;
    }

    private void showNoAchievementFragment()
    {
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right)
                .replace(R.id.fragment_container_result_1, new NoAchievementFragment())
                .addToBackStack(null)
                .commit();
    }
}