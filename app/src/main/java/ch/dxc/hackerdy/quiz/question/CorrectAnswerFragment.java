package ch.dxc.hackerdy.quiz.question;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import ch.dxc.hackerdy.R;
import ch.dxc.hackerdy.databinding.FragmentCorrectAnswerBinding;

public class CorrectAnswerFragment extends Fragment implements View.OnClickListener
{
    private static final String ARG_PARAM1 = "multiplier";

    private long mPoints;
    private int mMultiplier;

    public static CorrectAnswerFragment newInstance(int multiplier)
    {
        CorrectAnswerFragment fragment = new CorrectAnswerFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, multiplier);
        fragment.setArguments(args);

        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Intent data = getActivity().getIntent();
        mPoints = data.getLongExtra("points", 0);

        if (getArguments() != null)
        {
            mMultiplier = getArguments().getInt(ARG_PARAM1);
        }

        if (mMultiplier > 0)
        {
            mPoints *= mMultiplier;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        FragmentCorrectAnswerBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_correct_answer, container, false);

        binding.buttonContinue.setOnClickListener(this);

        binding.textPoints.setText(String.format("+ %s points!", mPoints));

        return binding.getRoot();
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.button_continue:
                Intent data = new Intent();
                data.putExtra("score", mPoints);
                data.putExtra("correct", true);

                getActivity().setResult(Activity.RESULT_OK, data);

                getActivity().finish();
                break;
        }
    }
}