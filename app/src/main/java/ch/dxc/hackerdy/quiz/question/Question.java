package ch.dxc.hackerdy.quiz.question;

import java.util.List;

public class Question
{
    private String mTopic;
    private String mQuestion;
    private List<String> mAnswerList;
    private String mSolution;
    private long mPoints;
    private String mReason;

    public Question(String topic, String question, List<String> answerList, String solution, long points, String reason)
    {
        mTopic = topic;
        mQuestion = question;
        mAnswerList = answerList;
        mSolution = solution;
        mPoints = points;
        mReason = reason;
    }

    public String getTopic()
    {
        return mTopic;
    }

    public String getQuestion()
    {
        return mQuestion;
    }

    public List<String> getAnswerList()
    {
        return mAnswerList;
    }

    public String getSolution()
    {
        return mSolution;
    }

    public long getPoints()
    {
        return mPoints;
    }

    public String getReason()
    {
        return mReason;
    }
}