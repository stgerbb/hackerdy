package ch.dxc.hackerdy.quiz.result;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import ch.dxc.hackerdy.R;
import ch.dxc.hackerdy.databinding.FragmentTrophiesUnlockedBinding;

public class TrophiesUnlockedFragment extends Fragment
{
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        FragmentTrophiesUnlockedBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_trophies_unlocked, container, false);
        return binding.getRoot();
    }
}