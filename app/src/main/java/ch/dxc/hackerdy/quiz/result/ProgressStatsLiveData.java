package ch.dxc.hackerdy.quiz.result;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;

import java.util.HashMap;
import java.util.Map;

public class ProgressStatsLiveData extends LiveData<Map<String, Object>> implements EventListener<DocumentSnapshot>
{
    private DocumentReference mRef;
    private ListenerRegistration mListener;

    public ProgressStatsLiveData(DocumentReference ref)
    {
        mRef = ref;
    }

    @Override
    protected void onActive()
    {
        mListener = mRef.addSnapshotListener(this);
    }

    @Override
    protected void onInactive()
    {
        mListener.remove();
    }

    @Override
    public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e)
    {
        if (documentSnapshot.exists() && documentSnapshot != null)
        {
            Map<String, Object> data = new HashMap<>();
            data.put("balance", documentSnapshot.getLong("balance"));
            data.put("level", documentSnapshot.getLong("level"));

            setValue(data);
        }
        else if (e != null)
        {
            // TODO Error handling
        }
    }
}