package ch.dxc.hackerdy.quiz;

public class QuizButtonMetadata
{
    private String mTopic;
    private long mPoints;

    public QuizButtonMetadata(String topic, long points)
    {
        mTopic = topic;
        mPoints = points;
    }

    public String getTopic()
    {
        return mTopic;
    }

    public long getPoints()
    {
        return mPoints;
    }
}