package ch.dxc.hackerdy.quiz.result;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.Map;

public class LevelDataLiveData extends LiveData<Map<String, Long>> implements EventListener<QuerySnapshot>
{
    private Query mQuery;
    private ListenerRegistration mListener;

    public LevelDataLiveData(Query query)
    {
        mQuery = query;
    }

    @Override
    protected void onActive()
    {
        mListener = mQuery.addSnapshotListener(this);
    }

    @Override
    protected void onInactive()
    {
        mListener.remove();
    }

    @Override
    public void onEvent(@Nullable QuerySnapshot querySnapshot, @Nullable FirebaseFirestoreException e)
    {
        if (!querySnapshot.isEmpty() && querySnapshot != null)
        {
            DocumentSnapshot snapshot = querySnapshot.getDocuments().get(0);

            Map<String, Long> data = new HashMap<>();
            data.put("level", snapshot.getLong("level"));
            data.put("points", snapshot.getLong("points"));

            setValue(data);
        }
        else if (e != null)
        {
            // TODO Error handling
        }
    }
}