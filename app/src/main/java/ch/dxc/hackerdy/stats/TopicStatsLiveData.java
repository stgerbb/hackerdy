package ch.dxc.hackerdy.stats;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;

import java.util.HashMap;
import java.util.Map;

public class TopicStatsLiveData extends LiveData<Map<String, Long>> implements EventListener<DocumentSnapshot>
{
    private DocumentReference mRef;
    private ListenerRegistration mListener;

    public TopicStatsLiveData(DocumentReference ref)
    {
        mRef = ref;
    }

    @Override
    protected void onActive()
    {
        mListener = mRef.addSnapshotListener(this);
    }

    @Override
    protected void onInactive()
    {
        mListener.remove();
    }

    @Override
    public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e)
    {
        if (documentSnapshot.exists() && documentSnapshot != null)
        {
            Map<String, Long> data = new HashMap<>();
            data.put("answered_correct", documentSnapshot.getLong("answered_correct"));
            data.put("answered_wrong", documentSnapshot.getLong("answered_wrong"));
            data.put("play_count", documentSnapshot.getLong("play_count"));

            setValue(data);
        }
        else if (e != null)
        {
            // TODO: Exception Handling
        }
    }
}