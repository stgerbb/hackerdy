package ch.dxc.hackerdy.stats;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;
import java.util.Map;

import ch.dxc.hackerdy.highscores.HighScoresListItem;
import ch.dxc.hackerdy.questionnaire.TopicListItem;
import ch.dxc.hackerdy.repositories.FirebaseRepository;

public class StatsViewModel extends ViewModel
{
    private MutableLiveData<Boolean> mLoadingTopics = new MutableLiveData<>();
    private MutableLiveData<Boolean> mLoadingRank = new MutableLiveData<>();
    private MutableLiveData<Boolean> mLoadingProfilePicture = new MutableLiveData<>();
    private MutableLiveData<Boolean> mLoadingUsername = new MutableLiveData<>();
    private MutableLiveData<Boolean> mLoadingBalance = new MutableLiveData<>();
    private MutableLiveData<Boolean> mLoadingPersonalBest = new MutableLiveData<>();
    private MutableLiveData<Boolean> mLoadingLevel = new MutableLiveData<>();
    private MutableLiveData<Boolean> mLoadingPlayTime = new MutableLiveData<>();
    private MutableLiveData<Boolean> mLoadingPlayedQuizzes = new MutableLiveData<>();
    private MutableLiveData<Boolean> mLoadingProgress = new MutableLiveData<>();
    private MutableLiveData<Boolean> mLoadingPerformance = new MutableLiveData<>();
    private String mId;

    FirebaseRepository mRepository = new FirebaseRepository();

    public LiveData<List<TopicListItem>> getTopics()
    {
        return mRepository.getTopics();
    }

    public LiveData<Map<String, Object>> getUserData()
    {
        setLoadingProfilePicture(true);
        setLoadingUsername(true);

        if (mId != null)
        {
            return mRepository.getUserData(mId);
        }

        return mRepository.getUserData(mRepository.getUser().getUid());
    }

    public LiveData<Map<String, Object>> getPerformanceStats()
    {
        setLoadingPersonalBest(true);
        setLoadingPlayedQuizzes(true);
        setLoadingPlayTime(true);
        setLoadingPerformance(true);

        if (mId != null)
        {
            return mRepository.getPerformanceStats(mId);
        }

        return mRepository.getPerformanceStats(mRepository.getUser().getUid());
    }

    public LiveData<Map<String, Object>> getProgressStats()
    {
        setLoadingBalance(true);
        setLoadingLevel(true);
        setLoadingProgress(true);

        if (mId != null)
        {
            return mRepository.getProgressStats(mId);
        }

        return mRepository.getProgressStats(mRepository.getUser().getUid());
    }

    public LiveData<Map<String, Long>> getLevelData(long level)
    {
        return mRepository.getLevelData(level);
    }

    public LiveData<Map<String, Long>> getTopicStats(String topic)
    {
        setLoadingTopics(true);

        if (mId != null)
        {
            return mRepository.getTopicStats(topic, mId);
        }

        return mRepository.getTopicStats(topic, mRepository.getUser().getUid());
    }

    public LiveData<List<HighScoresListItem>> getRank()
    {
        return mRepository.getHighScores();
    }

    public void setLoadingTopics(Boolean bool)
    {
        mLoadingTopics.setValue(bool);
    }

    public void setLoadingRank(Boolean bool)
    {
        mLoadingRank.setValue(bool);
    }

    public void setLoadingProfilePicture(Boolean bool)
    {
        mLoadingProfilePicture.setValue(bool);
    }

    public void setLoadingUsername(Boolean bool)
    {
        mLoadingUsername.setValue(bool);
    }

    public void setLoadingBalance(Boolean bool)
    {
        mLoadingBalance.setValue(bool);
    }

    public void setLoadingPersonalBest(Boolean bool)
    {
        mLoadingPersonalBest.setValue(bool);
    }

    public void setLoadingLevel(Boolean bool)
    {
        mLoadingLevel.setValue(bool);
    }

    public void setLoadingPlayTime(Boolean bool)
    {
        mLoadingPlayTime.setValue(bool);
    }

    public void setLoadingPlayedQuizzes(Boolean bool)
    {
        mLoadingPlayedQuizzes.setValue(bool);
    }

    public void setLoadingProgress(Boolean bool)
    {
        mLoadingProgress.setValue(bool);
    }

    public void setLoadingPerformance(Boolean bool)
    {
        mLoadingPerformance.setValue(bool);
    }

    public void setId(String id)
    {
        mId = id;
    }

    public LiveData<Boolean> isLoadingRank()
    {
        return mLoadingRank;
    }

    public LiveData<Boolean> isLoadingTopics()
    {
        return mLoadingTopics;
    }

    public LiveData<Boolean> isLoadingProfilePicture()
    {
        return mLoadingProfilePicture;
    }

    public LiveData<Boolean> isLoadingUsername()
    {
        return mLoadingUsername;
    }

    public LiveData<Boolean> isLoadingBalance()
    {
        return mLoadingBalance;
    }

    public LiveData<Boolean> isLoadingPersonalBest()
    {
        return mLoadingPersonalBest;
    }

    public LiveData<Boolean> isLoadingLevel()
    {
        return mLoadingLevel;
    }

    public LiveData<Boolean> isLoadingPlayTime()
    {
        return mLoadingPlayTime;
    }

    public LiveData<Boolean> isLoadingPlayedQuizzes()
    {
        return mLoadingPlayedQuizzes;
    }

    public LiveData<Boolean> isLoadingProgress()
    {
        return mLoadingProgress;
    }

    public LiveData<Boolean> isLoadingPerformance()
    {
        return mLoadingPerformance;
    }
}