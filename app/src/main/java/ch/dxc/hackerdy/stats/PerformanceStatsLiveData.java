package ch.dxc.hackerdy.stats;

import android.util.Log;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;

import java.util.HashMap;
import java.util.Map;

public class PerformanceStatsLiveData extends LiveData<Map<String, Object>> implements EventListener<DocumentSnapshot>
{
    private DocumentReference mRef;
    private ListenerRegistration mListener;

    public PerformanceStatsLiveData(DocumentReference ref)
    {
        mRef = ref;
    }

    @Override
    protected void onActive()
    {
        mListener = mRef.addSnapshotListener(this);
    }

    @Override
    protected void onInactive()
    {
        mListener.remove();
    }

    @Override
    public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e)
    {
        if (documentSnapshot.exists() && documentSnapshot != null)
        {
            Map<String, Object> data = new HashMap<>();
            data.put("answered_correct", documentSnapshot.getLong("answered_correct"));
            data.put("answered_wrong", documentSnapshot.getLong("answered_wrong"));

            Map<String, Object> personalBest = (Map<String, Object>) documentSnapshot.get("personal_best");
            if (!personalBest.isEmpty())
            {
                data.put("personal_best", documentSnapshot.get("personal_best"));
            }
            else
            {
                data.put("personal_best", "-");
            }

            data.put("play_time", documentSnapshot.getString("play_time"));
            data.put("played_quizzes", documentSnapshot.getLong("played_quizzes"));

            setValue(data);
        }
        else if (e != null)
        {
            // TODO: Error Handling
        }
    }
}