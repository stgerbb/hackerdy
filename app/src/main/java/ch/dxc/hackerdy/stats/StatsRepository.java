package ch.dxc.hackerdy.stats;

import androidx.lifecycle.LiveData;

import java.util.Map;

public interface StatsRepository
{
    LiveData<Map<String, Object>> getUserData(String id);

    LiveData<Map<String, Object>> getPerformanceStats(String id);

    LiveData<Map<String, Long>> getTopicStats(String topic, String id);
}