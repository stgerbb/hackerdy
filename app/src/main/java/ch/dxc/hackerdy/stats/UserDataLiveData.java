package ch.dxc.hackerdy.stats;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;

import java.util.HashMap;
import java.util.Map;

public class UserDataLiveData extends LiveData<Map<String, Object>> implements EventListener<DocumentSnapshot>
{
    private DocumentReference mRef;
    private ListenerRegistration mListener;

    public UserDataLiveData(DocumentReference ref)
    {
        mRef = ref;
    }

    @Override
    protected void onActive()
    {
        mListener = mRef.addSnapshotListener(this);
    }

    @Override
    protected void onInactive()
    {
        mListener.remove();
    }

    @Override
    public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e)
    {
        if (documentSnapshot != null)
        {
            Map<String, Object> data = new HashMap<>();
            data.put("email", documentSnapshot.getString("email"));
            data.put("photo", documentSnapshot.getString("photo"));
            data.put("username", documentSnapshot.getString("username"));
            data.put("submitted_feedback", documentSnapshot.getBoolean("submitted_feedback"));
            if (documentSnapshot.contains("new_email"))
            {
                data.put("new_email", documentSnapshot.getString("new_email"));
            }

            setValue(data);
        }
        else if (e != null)
        {
            // TODO: exception handling
        }
    }
}