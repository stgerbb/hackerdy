package ch.dxc.hackerdy.feedback;

import androidx.databinding.ObservableField;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import ch.dxc.hackerdy.R;
import ch.dxc.hackerdy.repositories.FirebaseRepository;

public class FeedbackViewModel extends ViewModel implements FeedbackCallback
{
    private static final int MAX_MESSAGE_LENGTH = 300;

    public final ObservableField<String> mSuggestions = new ObservableField<>("");
    private MutableLiveData<Boolean> mGoalAchievedError = new MutableLiveData<>();
    private MutableLiveData<Boolean> mInfoUsefulError = new MutableLiveData<>();
    private MutableLiveData<Boolean> mInputError = new MutableLiveData<>();
    private MutableLiveData<String> mRepositoryError = new MutableLiveData<>();
    private MutableLiveData<Boolean> mLoading = new MutableLiveData<>();
    private MutableLiveData<Boolean> mSubmitted = new MutableLiveData<>();
    private MutableLiveData<Integer> mSuggestionsError = new MutableLiveData<>();
    private String mGoalAchieved = null;
    private String mInfoUseful = null;
    private FirebaseRepository mRepository = new FirebaseRepository();

    public void submitFeedback(float ovalSatis)
    {
        boolean error = false;
        String suggestions = mSuggestions.get();

        if (mGoalAchieved == null)
        {
            mGoalAchievedError.setValue(true);
            error = true;
        }
        else
        {
            mGoalAchievedError.setValue(false);
        }

        if (mInfoUseful == null)
        {
            mInfoUsefulError.setValue(true);
            error = true;
        }
        else
        {
            mInfoUsefulError.setValue(false);
        }

        if (suggestions.length() > MAX_MESSAGE_LENGTH)
        {
            mSuggestionsError.setValue(R.string.message_too_long);
        }

        if (error)
        {
            mInputError.setValue(true);
        }
        else if (mGoalAchieved != null && mInfoUseful != null && suggestions.length() <= MAX_MESSAGE_LENGTH)
        {
            mRepository.submitFeedback(ovalSatis, mGoalAchieved, mInfoUseful, mSuggestions.get(), this);
        }
    }

    public void setGoalAchieved(String answer)
    {
        mGoalAchieved = answer;
    }

    public void setInfoUseful(String answer)
    {
        mInfoUseful = answer;
    }

    public void setLoading(Boolean bool)
    {
        mLoading.setValue(bool);
    }

    public LiveData<Boolean> getGoalReachedError()
    {
        return mGoalAchievedError;
    }

    public LiveData<Boolean> getInfoUsefulError()
    {
        return mInfoUsefulError;
    }

    public LiveData<Boolean> isLoading()
    {
        return mLoading;
    }

    public LiveData<String> getRepositoryError()
    {
        return mRepositoryError;
    }

    public LiveData<Boolean> isSubmitted()
    {
        return mSubmitted;
    }

    public LiveData<Boolean> isInputError()
    {
        return mInputError;
    }

    public LiveData<Integer> getSuggestionsError()
    {
        return mSuggestionsError;
    }

    @Override
    public void onStart()
    {
        setLoading(true);
    }

    @Override
    public void onSubmit()
    {
        mSubmitted.setValue(true);
    }

    @Override
    public void onFailure(String errorMsg)
    {
        mRepositoryError.setValue(errorMsg);
    }
}