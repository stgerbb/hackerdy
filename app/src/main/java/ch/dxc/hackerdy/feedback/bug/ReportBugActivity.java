package ch.dxc.hackerdy.feedback.bug;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.snackbar.Snackbar;

import ch.dxc.hackerdy.R;
import ch.dxc.hackerdy.databinding.ActivityReportBugBinding;
import ch.dxc.hackerdy.home.HomeActivity;

public class ReportBugActivity extends AppCompatActivity implements View.OnClickListener
{
    private ActivityReportBugBinding mBinding;
    private ReportBugViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_report_bug);

        mViewModel = ViewModelProviders.of(this).get(ReportBugViewModel.class);
        mBinding.setViewModel(mViewModel);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mBinding.inputDescription.setHint(R.string.hint_bug_description);

        mBinding.buttonSubmit.setOnClickListener(this);

        mBinding.inputDescription.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent)
            {
                if (view.getId() == R.id.input_description)
                {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (motionEvent.getAction() & MotionEvent.ACTION_MASK)
                    {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }

                return false;
            }
        });

        mBinding.inputDescription.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void afterTextChanged(Editable editable)
            {
                if (mBinding.inputLayoutDescription.isErrorEnabled())
                {
                    mBinding.inputLayoutDescription.setErrorEnabled(false);
                }
            }
        });

        mViewModel.isLoading().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean loading)
            {
                if (loading)
                {
                    mBinding.progressbarOnSubmit.bringToFront();
                    mBinding.progressbarOnSubmit.setVisibility(View.VISIBLE);
                    mBinding.inputDescription.setEnabled(false);
                    mBinding.buttonSubmit.setEnabled(false);
                }
                else
                {
                    mBinding.progressbarOnSubmit.setVisibility(View.GONE);
                    mBinding.inputDescription.setEnabled(true);
                    mBinding.buttonSubmit.setEnabled(true);
                }
            }
        });

        mViewModel.getDescriptionError().observe(this, new Observer<Integer>()
        {
            @Override
            public void onChanged(Integer resId)
            {
                mBinding.inputLayoutDescription.setError(getString(resId));
            }
        });

        mViewModel.getRepositoryError().observe(this, new Observer<String>()
        {
            @Override
            public void onChanged(String error)
            {
                Snackbar.make(mBinding.activityBug, R.string.error_in_processing_the_request, Snackbar.LENGTH_INDEFINITE).setAction(R.string.button_ok, new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                    }
                }).show();
            }
        });

        mViewModel.isSubmitted().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean submitted)
            {
                mViewModel.setLoading(false);

                if (submitted)
                {
                    setResult(Activity.RESULT_OK, new Intent(ReportBugActivity.this, HomeActivity.class));
                    finish();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view)
    {
        if (view.getId() == R.id.button_submit)
        {
            mViewModel.submitBugReport();
        }
    }
}