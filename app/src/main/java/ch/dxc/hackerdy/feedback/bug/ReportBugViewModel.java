package ch.dxc.hackerdy.feedback.bug;

import androidx.databinding.ObservableField;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import ch.dxc.hackerdy.R;
import ch.dxc.hackerdy.repositories.FirebaseRepository;

public class ReportBugViewModel extends ViewModel implements ReportBugCallback
{
    private static final int MAX_MESSAGE_LENGTH = 300;

    public final ObservableField<String> mDescription = new ObservableField<>("");
    private MutableLiveData<Integer> mDescriptionError = new MutableLiveData<>();
    private MutableLiveData<Boolean> mLoading = new MutableLiveData<>();
    private MutableLiveData<String> mRepositoryError = new MutableLiveData<>();
    private MutableLiveData<Boolean> mSubmitted = new MutableLiveData<>();
    private FirebaseRepository mRepository = new FirebaseRepository();

    public void submitBugReport()
    {
        String description = mDescription.get();

        if (description.length() == 0)
        {
            mDescriptionError.setValue(R.string.error_empty_description);
        }
        else if (description.length() > MAX_MESSAGE_LENGTH)
        {
            mDescriptionError.setValue(R.string.message_too_long);
        }
        else
        {
            mRepository.submitBugReport(description, this);
        }
    }

    public LiveData<Integer> getDescriptionError()
    {
        return mDescriptionError;
    }

    public LiveData<Boolean> isLoading()
    {
        return mLoading;
    }

    public void setLoading(Boolean bool)
    {
        mLoading.setValue(bool);
    }

    public LiveData<Boolean> isSubmitted()
    {
        return mSubmitted;
    }

    public LiveData<String> getRepositoryError()
    {
        return mRepositoryError;
    }

    @Override
    public void onStart()
    {
        setLoading(true);
    }

    @Override
    public void onSubmit()
    {
        mSubmitted.setValue(true);
    }

    @Override
    public void onFailure(String errorMsg)
    {
        mRepositoryError.setValue(errorMsg);
    }
}