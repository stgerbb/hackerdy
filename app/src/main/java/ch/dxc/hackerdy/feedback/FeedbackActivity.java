package ch.dxc.hackerdy.feedback;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import ch.dxc.hackerdy.R;
import ch.dxc.hackerdy.databinding.ActivityFeedbackBinding;
import ch.dxc.hackerdy.home.HomeActivity;

public class FeedbackActivity extends AppCompatActivity implements View.OnClickListener
{
    private ActivityFeedbackBinding mBinding;
    private FeedbackViewModel mViewModel;
    private List<MaterialButton> mGoalAchievedBtnList = new ArrayList<>();
    private List<MaterialButton> mInfoUsefulBtnList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_feedback);

        mViewModel = ViewModelProviders.of(this).get(FeedbackViewModel.class);
        mBinding.setViewModel(mViewModel);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mGoalAchievedBtnList.add(mBinding.buttonGoalReachedYes);
        mGoalAchievedBtnList.add(mBinding.buttonGoalReachedPartially);
        mGoalAchievedBtnList.add(mBinding.buttonGoalReachedNo);

        mInfoUsefulBtnList.add(mBinding.buttonInfoUsefulYes);
        mInfoUsefulBtnList.add(mBinding.buttonInfoUsefulNo);

        mBinding.buttonGoalReachedYes.setOnClickListener(this);
        mBinding.buttonGoalReachedPartially.setOnClickListener(this);
        mBinding.buttonGoalReachedNo.setOnClickListener(this);

        mBinding.buttonInfoUsefulYes.setOnClickListener(this);
        mBinding.buttonInfoUsefulNo.setOnClickListener(this);

        mBinding.buttonSubmit.setOnClickListener(this);

        mBinding.inputSuggestions.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent)
            {
                if (view.getId() == R.id.input_suggestions)
                {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (motionEvent.getAction() & MotionEvent.ACTION_MASK)
                    {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }

                return false;
            }
        });

        mBinding.inputSuggestions.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void afterTextChanged(Editable editable)
            {
                if (mBinding.layoutInputSuggestions.isErrorEnabled())
                {
                    mBinding.layoutInputSuggestions.setErrorEnabled(false);
                }
            }
        });

        mViewModel.getGoalReachedError().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean error)
            {
                if (error)
                {
                    showErrorOnEmptyFields(mBinding.textGoalAchieved);
                }
                else
                {
                    hideError(mBinding.textGoalAchieved);
                }
            }
        });

        mViewModel.getInfoUsefulError().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean error)
            {
                if (error)
                {
                    showErrorOnEmptyFields(mBinding.textInformationUseful);
                }
                else
                {
                    hideError(mBinding.textInformationUseful);
                }
            }
        });

        mViewModel.isLoading().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean loading)
            {
                if (loading)
                {
                    mBinding.progressbarOnSubmit.bringToFront();
                    mBinding.progressbarOnSubmit.setVisibility(View.VISIBLE);

                    disableButtons(mGoalAchievedBtnList);
                    disableButtons(mInfoUsefulBtnList);

                    mBinding.ratingbarOvalSatis.setEnabled(false);
                    mBinding.buttonSubmit.setEnabled(false);
                }
                else
                {
                    mBinding.progressbarOnSubmit.setVisibility(View.GONE);

                    enableButtons(mGoalAchievedBtnList);
                    enableButtons(mInfoUsefulBtnList);

                    mBinding.ratingbarOvalSatis.setEnabled(true);
                    mBinding.buttonSubmit.setEnabled(true);
                }
            }
        });

        mViewModel.isInputError().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean inputError)
            {
                if (inputError)
                {
                    Snackbar.make(mBinding.activityFeedback, R.string.required_fields, Snackbar.LENGTH_INDEFINITE).setAction(R.string.button_hide, new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View view)
                        {
                        }
                    }).show();
                }
            }
        });

        mViewModel.getRepositoryError().observe(this, new Observer<String>()
        {
            @Override
            public void onChanged(String error)
            {
                Snackbar.make(mBinding.activityFeedback, R.string.error_in_processing_the_request, Snackbar.LENGTH_INDEFINITE).setAction(R.string.button_hide, new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                    }
                }).show();
            }
        });

        mViewModel.isSubmitted().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean submitted)
            {
                mViewModel.setLoading(false);

                if (submitted)
                {
                    setResult(Activity.RESULT_OK, new Intent(FeedbackActivity.this, HomeActivity.class));
                    finish();
                }
            }
        });

        mViewModel.getSuggestionsError().observe(this, new Observer<Integer>()
        {
            @Override
            public void onChanged(Integer resId)
            {
                mBinding.layoutInputSuggestions.setError(getString(resId));
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view)
    {
        MaterialButton btn = (MaterialButton) view;
        MaterialButton pressedBtn;

        switch (view.getId())
        {
            case R.id.button_goal_reached_yes:
                pressedBtn = onClickRadioButton(btn, mGoalAchievedBtnList);
                onClickGoalReachedBtn(pressedBtn);
                break;
            case R.id.button_goal_reached_partially:
                pressedBtn = onClickRadioButton(btn, mGoalAchievedBtnList);
                onClickGoalReachedBtn(pressedBtn);
                break;
            case R.id.button_goal_reached_no:
                pressedBtn = onClickRadioButton(btn, mGoalAchievedBtnList);
                onClickGoalReachedBtn(pressedBtn);
                break;
            case R.id.button_info_useful_yes:
                pressedBtn = onClickRadioButton(btn, mInfoUsefulBtnList);
                onClickInfoUsefulBtn(pressedBtn);
                break;
            case R.id.button_info_useful_no:
                pressedBtn = onClickRadioButton(btn, mInfoUsefulBtnList);
                onClickInfoUsefulBtn(pressedBtn);
                break;
            case R.id.button_submit:
                mViewModel.submitFeedback(mBinding.ratingbarOvalSatis.getRating());
                break;
        }
    }

    private MaterialButton onClickRadioButton(MaterialButton view, List<MaterialButton> list)
    {
        view.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.colorYellow));
        view.setTextColor(ContextCompat.getColor(this, android.R.color.black));
        view.setIconTint(ContextCompat.getColorStateList(this, android.R.color.black));

        list.remove(view);

        for (MaterialButton button : list)
        {
            button.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.colorPrimaryDark));
            button.setTextColor(ContextCompat.getColor(this, android.R.color.white));
            button.setIconTint(ContextCompat.getColorStateList(this, android.R.color.white));
        }

        list.add(view);

        return view;
    }

    private void onClickGoalReachedBtn(MaterialButton pressedBtn)
    {
        mViewModel.setGoalAchieved(pressedBtn.getText().toString());
    }

    private void onClickInfoUsefulBtn(MaterialButton pressedBtn)
    {
        mViewModel.setInfoUseful(pressedBtn.getText().toString());
    }

    private void disableButtons(List<MaterialButton> list)
    {
        for (MaterialButton btn : list)
        {
            btn.setEnabled(false);
        }
    }

    private void enableButtons(List<MaterialButton> list)
    {
        for (MaterialButton btn : list)
        {
            btn.setEnabled(true);
        }
    }

    private void showErrorOnEmptyFields(TextView view)
    {
        view.setTextColor(ContextCompat.getColor(FeedbackActivity.this, android.R.color.holo_red_light));
    }

    private void hideError(TextView view)
    {
        view.setTextColor(ContextCompat.getColor(FeedbackActivity.this, android.R.color.white));
    }
}