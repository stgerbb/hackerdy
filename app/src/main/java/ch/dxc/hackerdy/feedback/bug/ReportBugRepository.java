package ch.dxc.hackerdy.feedback.bug;

public interface ReportBugRepository
{
    void submitBugReport(String description, ReportBugCallback bug);
}