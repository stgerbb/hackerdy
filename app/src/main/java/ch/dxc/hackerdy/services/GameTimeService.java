package ch.dxc.hackerdy.services;

import android.app.Service;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Binder;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.util.Log;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.TextView;

import ch.dxc.hackerdy.R;

public class GameTimeService extends Service
{
    private final IBinder mBinder = new LocalBinder();
    private CountDownTimer mTimer;
    private int mSeconds = 0;
    private int mMinutes = 0;

    @Override
    public IBinder onBind(Intent intent)
    {
        return mBinder;
    }

    public void startTimer(final TextView view)
    {
        mTimer = new CountDownTimer(302000, 1000)
        {
            @Override
            public void onTick(long l)
            {
                if (mMinutes == 4 && mSeconds == 40)
                {
                    view.setTextColor(getResources().getColor(R.color.colorYellow));
                    Drawable drawable = view.getCompoundDrawables()[0];
                    drawable.setColorFilter(getResources().getColor(R.color.colorYellow), PorterDuff.Mode.SRC_IN);

//                    Makes the text blink
                    Animation anim = new AlphaAnimation(0.0f, 1.0f);
                    anim.setDuration(500);
                    anim.setStartOffset(0);
                    anim.setRepeatMode(Animation.REVERSE);
                    anim.setRepeatCount(Animation.INFINITE);
                    view.startAnimation(anim);
                }

                if (mSeconds >= 0 && mSeconds < 10)
                {
                    if (mMinutes >= 0 && mMinutes < 10)
                    {
                        view.setText(String.format("0%s:0%s", String.valueOf(mMinutes), String.valueOf(mSeconds)));
                    }
                    else
                    {
                        view.setText(String.format("%s:0%s", String.valueOf(mMinutes), String.valueOf(mSeconds)));
                    }
                }
                else if (mSeconds >= 10 && mSeconds <= 59)
                {
                    if (mMinutes >= 0 && mMinutes < 10)
                    {
                        view.setText(String.format("0%s:%s", String.valueOf(mMinutes), String.valueOf(mSeconds)));
                    }
                    else
                    {
                        view.setText(String.format("%s:%s", String.valueOf(mMinutes), String.valueOf(mSeconds)));
                    }
                }
                else if (mSeconds == 60)
                {
                    mSeconds = 0;
                    mMinutes++;

                    if (mMinutes >= 0 && mMinutes < 10)
                    {
                        view.setText(String.format("0%s:0%s", String.valueOf(mMinutes), String.valueOf(mSeconds)));
                    }
                    else
                    {
                        view.setText(String.format("%s:0%s", String.valueOf(mMinutes), String.valueOf(mSeconds)));
                    }
                }

                mSeconds++;
            }

            @Override
            public void onFinish()
            {
                Log.i("Log", "Timer finished!");
            }
        }.start();
    }

    public void stopTimer()
    {
        mTimer.cancel();
    }

    public void restartTimer()
    {
        mTimer.start();
    }

    public int getSeconds()
    {
        return mSeconds;
    }

    public int getMinutes()
    {
        return mMinutes;
    }

    public class LocalBinder extends Binder
    {
        public GameTimeService getService()
        {
            return GameTimeService.this;
        }
    }
}