package ch.dxc.hackerdy.registration;

import com.google.firebase.auth.FirebaseUser;

public interface UploadCallback
{
    void onSuccess(FirebaseUser user, String photoUrl);

    void onFailure(String msg);
}