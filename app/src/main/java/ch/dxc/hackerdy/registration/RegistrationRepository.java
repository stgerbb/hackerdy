package ch.dxc.hackerdy.registration;

import com.google.firebase.auth.FirebaseUser;

public interface RegistrationRepository
{
    void createUserWithEmailAndPassword(String username, String email, String password, byte[] photo, RegistrationCallback registration);

    void persistUser(FirebaseUser user, String photoUrl, RegistrationCallback registration);

    void uploadPhoto(FirebaseUser user, byte[] photo, UploadCallback upload);
}