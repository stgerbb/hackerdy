package ch.dxc.hackerdy.registration;

import android.app.Application;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Patterns;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.io.File;
import java.io.InputStream;

import ch.dxc.hackerdy.R;
import ch.dxc.hackerdy.repositories.FirebaseRepository;
import ch.dxc.hackerdy.util.FileUtil;
import ch.dxc.hackerdy.util.ImageUtil;
import ch.dxc.hackerdy.util.Regex;
import id.zelory.compressor.Compressor;

public class RegistrationViewModel extends AndroidViewModel implements RegistrationCallback
{
    private static final long ONE_MB = 1000000;

    public final ObservableField<String> mUsername = new ObservableField<>("");
    public final ObservableField<String> mEmail = new ObservableField<>("");
    public final ObservableField<String> mPassword = new ObservableField<>("");
    public final ObservableField<String> mConfirmPassword = new ObservableField<>("");

    private MutableLiveData<Boolean> mLoading = new MutableLiveData<>();
    private MutableLiveData<Integer> mUsernameError = new MutableLiveData<>();
    private MutableLiveData<Integer> mEmailError = new MutableLiveData<>();
    private MutableLiveData<Integer> mPasswordError = new MutableLiveData<>();
    private MutableLiveData<Integer> mConfirmPasswordError = new MutableLiveData<>();
    private MutableLiveData<String> mRepositoryError = new MutableLiveData<>();
    private MutableLiveData<Boolean> mRegistrationSuccessful = new MutableLiveData<>();
    private MutableLiveData<Bitmap> mPhoto = new MutableLiveData<>();
    private MutableLiveData<String> mPhotoError = new MutableLiveData<>();
    private MutableLiveData<String> mUndo = new MutableLiveData<>();

    private FirebaseRepository mRepository = new FirebaseRepository();
    private byte[] mPhotoArray;

    public RegistrationViewModel(@NonNull Application application)
    {
        super(application);
    }

    void createUserWithEmailAndPassword()
    {
        String username = mUsername.get().trim();
        String email = mEmail.get().trim();
        String password = mPassword.get();
        String confirmPassword = mConfirmPassword.get();

        if (username.isEmpty())
        {
            mUsernameError.setValue(R.string.error_empty_username);
        }
        else if (!username.matches(Regex.DISPLAY_NAME.getRegex()))
        {
            mUsernameError.setValue(R.string.error_username_regex);
        }
        else if (email.isEmpty())
        {
            mEmailError.setValue(R.string.error_empty_email);
        }
        else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches())
        {
            mEmailError.setValue(R.string.error_email_format);
        }
        else if (password.isEmpty())
        {
            mPasswordError.setValue(R.string.error_empty_password);
        }
        else if (confirmPassword.isEmpty())
        {
            mConfirmPasswordError.setValue(R.string.error_empty_password);
        }
        else if (password.matches(Regex.PASSWORD.getRegex()))
        {
            mPasswordError.setValue(R.string.error_password_regex);
        }
        else if (!password.equals(confirmPassword) || !confirmPassword.equals(password))
        {
            mConfirmPasswordError.setValue(R.string.error_passwords_do_not_match);
        }
        else
        {
            if (mPhotoArray != null)
            {
                mRepository.createUserWithEmailAndPassword(username, email, password, mPhotoArray, this);
            }
            else
            {
                ImageUtil image = new ImageUtil();

                Drawable photo = ContextCompat.getDrawable(getApplication(), R.drawable.portrait);
                mPhotoArray = image.convertDrawableToByteArray(photo);

                mRepository.createUserWithEmailAndPassword(username, email, password, mPhotoArray, this);
            }
        }
    }

    void updatePhoto(Intent intent, Compressor compressor)
    {
        try
        {
            FileUtil fileUtil = new ch.dxc.hackerdy.util.FileUtil();
            File file = fileUtil.createTempFile("photo", ".png");
            InputStream inputStream = getApplication().getContentResolver().openInputStream(intent.getData());
            fileUtil.copyInputStreamToFile(file, inputStream);
            long fileLength = file.length();

            if (fileLength > ONE_MB)
            {
                mPhotoError.setValue(String.format("%s %s %s", getApplication().getString(R.string.image_too_big_1), fileUtil.fileLengthToMb(fileLength), getApplication().getString(R.string.image_too_big_2)));
            }
            else
            {
                File compressedFile = compressor.compressToFile(file);
                mPhotoArray = fileUtil.fileToByteArray(compressedFile);
                Bitmap compressedBitmap = compressor.compressToBitmap(compressedFile);
                mPhoto.setValue(compressedBitmap);
            }
        } catch (Exception e)
        {
            mPhotoError.setValue(getApplication().getString(R.string.error_in_processing_the_request));
        }
    }

    void setUndo(String text)
    {
        mUndo.setValue(text);
    }

    void setPhoto(Bitmap photo)
    {
        mPhoto.setValue(photo);
    }

    @Override
    public void onStart()
    {
        mLoading.setValue(true);
    }

    @Override
    public void onSuccess()
    {
        mLoading.setValue(false);
        mRegistrationSuccessful.setValue(true);
    }

    @Override
    public void onFailure(String msg)
    {
        mLoading.setValue(false);
        mRepositoryError.setValue(msg);
    }

    @Override
    public void usernameAlreadyExists()
    {
        mLoading.setValue(false);
        mUsernameError.setValue(R.string.error_username_already_in_use);
    }

    public LiveData<Integer> getUsernameError()
    {
        return mUsernameError;
    }

    public LiveData<Integer> getEmailError()
    {
        return mEmailError;
    }

    public LiveData<Integer> getPasswordError()
    {
        return mPasswordError;
    }

    public LiveData<Boolean> isLoading()
    {
        return mLoading;
    }

    public LiveData<String> getRepositoryError()
    {
        return mRepositoryError;
    }

    public LiveData<Boolean> getRegistrationSuccessful()
    {
        return mRegistrationSuccessful;
    }

    public LiveData<Bitmap> getPhoto()
    {
        return mPhoto;
    }

    public LiveData<String> getPhotoError()
    {
        return mPhotoError;
    }

    public LiveData<String> getUndo()
    {
        return mUndo;
    }

    public LiveData<Integer> getConfirmPasswordError()
    {
        return mConfirmPasswordError;
    }
}