package ch.dxc.hackerdy.home;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.android.gms.auth.api.signin.GoogleSignInClient;

import java.util.Map;

import ch.dxc.hackerdy.repositories.FirebaseRepository;

public class HomeViewModel extends ViewModel
{
    private MutableLiveData<Boolean> mLoadingProfilePicture = new MutableLiveData<>();
    private MutableLiveData<Boolean> mLoadingUsername = new MutableLiveData<>();
    private MutableLiveData<Boolean> mLoadingEmail = new MutableLiveData<>();


    private FirebaseRepository mRepository = new FirebaseRepository();

    public LiveData<Map<String, Object>> getUserData()
    {
        setLoadingProfilePicture(true);
        setLoadingUsername(true);
        setLoadingEmail(true);

        return mRepository.getUserData(mRepository.getUser().getUid());
    }

    public void signOut(GoogleSignInClient googleClient)
    {
        mRepository.signOut(googleClient);
    }

    public LiveData<Boolean> isLoadingProfilePicture()
    {
        return mLoadingProfilePicture;
    }

    public LiveData<Boolean> isLoadingUsername()
    {
        return mLoadingUsername;
    }

    public LiveData<Boolean> isLoadingEmail()
    {
        return mLoadingEmail;
    }

    public void setLoadingProfilePicture(Boolean bool)
    {
        mLoadingProfilePicture.setValue(bool);
    }

    public void setLoadingEmail(Boolean bool)
    {
        mLoadingEmail.setValue(bool);
    }

    public void setLoadingUsername(Boolean bool)
    {
        mLoadingUsername.setValue(bool);
    }
}