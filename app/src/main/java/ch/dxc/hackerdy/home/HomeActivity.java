package ch.dxc.hackerdy.home;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.Map;

import ch.dxc.hackerdy.R;
import ch.dxc.hackerdy.databinding.ActivityHomeBinding;
import ch.dxc.hackerdy.feedback.FeedbackActivity;
import ch.dxc.hackerdy.feedback.bug.ReportBugActivity;
import ch.dxc.hackerdy.gamemode.GameModeActivity;
import ch.dxc.hackerdy.highscores.HighScoresActivity;
import ch.dxc.hackerdy.login.LoginActivity;
import ch.dxc.hackerdy.settings.SettingsActivity;
import ch.dxc.hackerdy.stats.StatsActivity;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener
{
    private static final int RC_SUBMIT_FEEDBACK = 1;
    private static final int RC_BUG_REPORT = 2;

    private ActivityHomeBinding mBinding;
    private HomeViewModel mViewModel;
    private GoogleSignInClient mGoogleClient;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        mViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);

        setSupportActionBar(mBinding.toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mBinding.drawer, mBinding.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mBinding.drawer.addDrawerListener(toggle);
        toggle.syncState();

        mBinding.navView.setNavigationItemSelectedListener(this);
        mBinding.navView.setCheckedItem(R.id.menu_main_menu);

        final Menu navMenu = mBinding.navView.getMenu();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestIdToken(getString(R.string.default_web_client_id)).requestEmail().build();
        mGoogleClient = GoogleSignIn.getClient(this, gso);

        mBinding.buttonNewQuiz.setOnClickListener(this);
        mBinding.buttonStats.setOnClickListener(this);
        mBinding.buttonKnowledgeCenter.setOnClickListener(this);
        mBinding.buttonNews.setOnClickListener(this);

        mViewModel.getUserData().observe(this, new Observer<Map<String, Object>>()
        {
            @Override
            public void onChanged(Map<String, Object> userDataMap)
            {
                ImageView photo = mBinding.navView.getHeaderView(0).findViewById(R.id.image_photo);
                Picasso.get().load(Uri.parse(userDataMap.get("photo").toString())).into(photo, new Callback()
                {
                    @Override
                    public void onSuccess()
                    {
                        mViewModel.setLoadingProfilePicture(false);
                    }

                    @Override
                    public void onError(Exception e)
                    {
                    }
                });

                TextView lblUsername = mBinding.navView.getHeaderView(0).findViewById(R.id.text_username);
                lblUsername.setText(userDataMap.get("username").toString());
                mViewModel.setLoadingUsername(false);

                TextView lblEmail = mBinding.navView.getHeaderView(0).findViewById(R.id.text_email);
                lblEmail.setText(userDataMap.get("email").toString());
                mViewModel.setLoadingEmail(false);

                boolean submittedFeedback = (Boolean) userDataMap.get("submitted_feedback");
                if (submittedFeedback)
                {
                    MenuItem item = navMenu.findItem(R.id.menu_submit_feedback);
                    item.setTitle(R.string.menu_thank_you_for_your_feedback);
                    item.setEnabled(false);
                    item.setIcon(ContextCompat.getDrawable(HomeActivity.this, R.drawable.ic_insert_emoticon_black_24dp));
                }
            }
        });

        mViewModel.isLoadingProfilePicture().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean loading)
            {
                ProgressBar progressbar = mBinding.navView.getHeaderView(0).findViewById(R.id.progressbar_photo);
                if (loading)
                {
                    progressbar.setVisibility(View.VISIBLE);
                }
                else
                {
                    progressbar.setVisibility(View.GONE);
                }
            }
        });

        mViewModel.isLoadingUsername().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean loading)
            {
                ProgressBar progressbar = mBinding.navView.getHeaderView(0).findViewById(R.id.progressbar_username);
                if (loading)
                {
                    progressbar.setVisibility(View.VISIBLE);
                }
                else
                {
                    progressbar.setVisibility(View.GONE);
                }
            }
        });

        mViewModel.isLoadingEmail().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean loading)
            {
                ProgressBar progressbar = mBinding.navView.getHeaderView(0).findViewById(R.id.progressbar_email);
                if (loading)
                {
                    progressbar.setVisibility(View.VISIBLE);
                }
                else
                {
                    progressbar.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        if (mBinding.drawer.isDrawerOpen(GravityCompat.START))
        {
            mBinding.drawer.closeDrawer(GravityCompat.START);
        }
        else
        {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.menu_sign_out:
                mViewModel.signOut(mGoogleClient);
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                break;
            case R.id.menu_new_quiz:
                startActivity(new Intent(this, GameModeActivity.class));
                closeNavDrawer();
                break;
            case R.id.menu_stats:
                startActivity(new Intent(this, StatsActivity.class));
                closeNavDrawer();
                break;
            case R.id.menu_high_scores:
                startActivity(new Intent(this, HighScoresActivity.class));
                closeNavDrawer();
                break;
            case R.id.menu_bug_report:
                startActivityForResult(new Intent(this, ReportBugActivity.class), RC_BUG_REPORT);
                closeNavDrawer();
                break;
            case R.id.menu_submit_feedback:
                startActivityForResult(new Intent(this, FeedbackActivity.class), RC_SUBMIT_FEEDBACK);
                closeNavDrawer();
                break;
            case R.id.menu_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                closeNavDrawer();
                break;
        }

        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == R.id.settings_menu)
        {
            startActivity(new Intent(this, SettingsActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.settings_menu, menu);

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SUBMIT_FEEDBACK)
        {
            if (resultCode == Activity.RESULT_OK)
            {
                Snackbar.make(mBinding.activityHome, R.string.thank_you_for_your_feedback, Snackbar.LENGTH_INDEFINITE).setAction(R.string.button_hide, new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                    }
                }).show();
            }
        }
        else if (requestCode == RC_BUG_REPORT)
        {
            if (resultCode == Activity.RESULT_OK)
            {
                Snackbar.make(mBinding.activityHome, R.string.thank_you_for_your_bug_report, Snackbar.LENGTH_INDEFINITE).setAction(R.string.button_hide, new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                    }
                }).show();
            }
        }
    }

    // TODO: Laggy in App
    private void closeNavDrawer()
    {
        if (mBinding.drawer.isDrawerOpen(GravityCompat.START))
        {
            mBinding.drawer.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.button_new_quiz:
                startActivity(new Intent(this, GameModeActivity.class));
                break;
            case R.id.button_stats:
                startActivity(new Intent(this, StatsActivity.class));
                break;
            case R.id.button_knowledge_center:
                Snackbar.make(mBinding.activityHome, "Under construction...", Snackbar.LENGTH_LONG).show();
                break;
            case R.id.button_news:
                Snackbar.make(mBinding.activityHome, "Under construction...", Snackbar.LENGTH_LONG).show();
                break;
        }
    }
}