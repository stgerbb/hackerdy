package ch.dxc.hackerdy.util;

public enum Regex
{
    //    String can't start with an _, numeric or special character, must contain only alphanumerics and consist between 3 and 20 characters
    DISPLAY_NAME("^[^_0-9\\W][a-zA-Z0-9]{3,19}$"),

    //    anything with less than 12 characters OR anything with no numbers OR anything with no uppercase OR anything with no special characters is an invalid password
    PASSWORD("^(.{0,11}|[^0-9]*|[^A-Z]*|[a-zA-Z0-9]*)$"),

    //    String must contain 1 upper case letter and 1 numeric
    ONE_UPPER_ONE_NUMERIC("^(?=.*[A-Z])(?=.*\\d).+$"),

    //    String must contain 1 upper case letter and 1 numeric, but no special character
    ONE_UPPER_ONE_NUMMERIC_NO_SPECIAL("^(?=.*[A-Z])(?=.*\\d)(?!.*\\W).+$"),

    //    String must contain 1 upper case letter, 1 numeric and 1 special character
    ONE_UPPER_ONE_NUMERIC_ONE_SPECIAL("^(?=.*\\d)(?=.*[A-Z])(?=.*\\W).+$"),

    //    String must contain 1 upper case letter, no numeric and no special character
    ONE_UPPER_NO_NUMERIC_NO_SPECIAL("^(?=.*[A-Z])(?!.*\\d)(?!.*\\W).+$"),

    //    String must contain 1 numeric, no upper case letter and no special character
    ONE_NUMERIC_NO_UPPER_NO_SPECIAL("^(?=.*\\d)(?!.*[A-Z])(?!.*\\W).+$"),

    //    String must contain 1 special character, no upper case letter and no numeric
    ONE_SPECIAL_NO_UPPER_NO_NUMERIC("^(?=.*\\W)(?!.*[A-Z])(?!.*\\d).+$"),

    //    String must contain 1 upper case letter, 1 special character and no numeric
    ONE_UPPER_ONE_SPECIAL_NO_NUMERIC("^(?=.*[A-Z])(?=.*\\W)(?!.*\\d).+$"),

    //    String must contain 1 numeric, 1 special character and no upper case letter
    ONE_NUMERIC_ONE_SPECIAL_NO_UPPER("^(?=.*\\d)(?=.*\\W)(?!.*[A-Z]).+$");

    private final String regex;

    Regex(String regex)
    {
        this.regex = regex;
    }

    public String getRegex()
    {
        return regex;
    }
}