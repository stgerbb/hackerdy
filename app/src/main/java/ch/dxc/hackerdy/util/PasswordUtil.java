package ch.dxc.hackerdy.util;

import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import ch.dxc.hackerdy.R;

public class PasswordUtil
{
    private int mProgress;
    private int mStringId;

    public void showPasswordStrengthMeter(TextView textStrength, ProgressBar progressbar, Editable editable)
    {
        progressbar.setVisibility(View.VISIBLE);
        textStrength.setVisibility(View.VISIBLE);

        String password = editable.toString();
        if (password.length() == 0)
        {
            progressbar.setVisibility(View.GONE);
            textStrength.setVisibility(View.GONE);
        }
        else if (password.length() < 8)
        {
            changeProgressBarColor(progressbar, Color.RED);
            setPasswordStrengthIndicator(R.string.text_weak, 25);
        }
        else if (password.length() >= 8 && password.length() < 12)
        {
            if (password.matches(Regex.ONE_UPPER_ONE_NUMERIC_ONE_SPECIAL.getRegex()))
            {
                changeProgressBarColor(progressbar, Color.rgb(255, 165, 0)); // Orange
                setPasswordStrengthIndicator(R.string.text_reasonable, 63);
            }
            else
            {
                measurePasswordStrength(progressbar, password);
            }

        }
        else if (password.length() >= 12 && password.length() < 16)
        {
            if (password.matches(Regex.ONE_UPPER_ONE_NUMERIC_ONE_SPECIAL.getRegex()))
            {
                changeProgressBarColor(progressbar, Color.GREEN);
                setPasswordStrengthIndicator(R.string.text_acceptable, 75);
            }
            else
            {
                measurePasswordStrength(progressbar, password);
            }

        }
        else if (password.length() >= 16)
        {
            if (password.matches(Regex.ONE_UPPER_ONE_NUMERIC_ONE_SPECIAL.getRegex()))
            {
                changeProgressBarColor(progressbar, Color.CYAN);
                setPasswordStrengthIndicator(R.string.text_strong, 100);
            }
            else
            {
                measurePasswordStrength(progressbar, password);
            }
        }
    }

    private void measurePasswordStrength(ProgressBar progressbar, String password)
    {
        if (password.matches(Regex.ONE_UPPER_NO_NUMERIC_NO_SPECIAL.getRegex()))
        {
            changeProgressBarColor(progressbar, Color.RED);
            setPasswordStrengthIndicator(R.string.text_weak, 38);
        }
        else if (password.matches(Regex.ONE_NUMERIC_NO_UPPER_NO_SPECIAL.getRegex()))
        {
            changeProgressBarColor(progressbar, Color.RED);
            setPasswordStrengthIndicator(R.string.text_weak, 38);
        }
        else if (password.matches(Regex.ONE_SPECIAL_NO_UPPER_NO_NUMERIC.getRegex()))
        {
            changeProgressBarColor(progressbar, Color.RED);
            setPasswordStrengthIndicator(R.string.text_weak, 38);
        }
        else if (password.matches(Regex.ONE_UPPER_ONE_NUMERIC.getRegex()))
        {
            changeProgressBarColor(progressbar, Color.rgb(255, 165, 0)); // Orange
            setPasswordStrengthIndicator(R.string.text_medium, 50);
        }
        else if (password.matches(Regex.ONE_UPPER_ONE_SPECIAL_NO_NUMERIC.getRegex()))
        {
            changeProgressBarColor(progressbar, Color.rgb(255, 165, 0)); // Orange
            setPasswordStrengthIndicator(R.string.text_medium, 50);
        }
        else if (password.matches(Regex.ONE_NUMERIC_ONE_SPECIAL_NO_UPPER.getRegex()))
        {
            changeProgressBarColor(progressbar, Color.rgb(255, 165, 0)); // Orange
            setPasswordStrengthIndicator(R.string.text_medium, 50);
        }
        else
        {
            changeProgressBarColor(progressbar, Color.RED);
            setPasswordStrengthIndicator(R.string.text_weak, 25);
        }
    }

    private void changeProgressBarColor(ProgressBar progressbar, int color)
    {
        Drawable drawable = progressbar.getProgressDrawable();
        drawable.setColorFilter(new LightingColorFilter(0xFF000000, color));
    }

    private void setPasswordStrengthIndicator(int stringId, int progress)
    {
        mProgress = progress;
        mStringId = stringId;
    }

    public int getProgress()
    {
        return mProgress;
    }

    public int getStringId()
    {
        return mStringId;
    }
}